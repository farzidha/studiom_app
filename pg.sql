 drop database studiom_db;

create database if not exists studiom_db;

use studiom_db;

create table members(id integer primary key auto_increment, name varchar(50),address varchar (150),contact varchar(25),mail varchar(30),gender varchar(10));

create table services(id integer primary key auto_increment, name varchar(50),price integer);

create table staff(id integer primary key auto_increment, name varchar(50),address varchar (150),contact varchar(25),mail varchar(30),gender varchar(10));


create table billing(id integer primary key auto_increment,mem_name varchar(50), mem_phone varchar(15), billing_date varchar(20),sub_total integer, discount decimal, tax decimal, total decimal);
create table bill_items(id integer primary key auto_increment, bill_id integer, service varchar(50), amount integer, qty integer, staff varchar(50), total integer);

create table expense(id integer primary key auto_increment,type varchar(20),paid_date date,amount integer(20), staff varchar(50));

create table messages(id integer primary key auto_increment, text varchar(160), msg_date varchar(20));

alter table billing add(pay_mode varchar(10) default "Cash");