/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author bluroe
 */
public class MemberFilterTable extends AbstractTableModel{

     private static final String[] columnNames={"Member Id","Name","Address","Contact", "Mail", "Gender"};
    private static final Class [] columnClasses={int.class,String.class,String.class,String.class,String.class,String.class};
    private ArrayList<Person> memberReport;

    MemberFilterTable(ArrayList<Person> memberfilter) {
       this.memberReport=memberfilter;
    }
    
    public String getColumnName(int col){
    return  columnNames[col];
    }
    
     public Class<?> getColumnClass(int col){
        return columnClasses[col];
    }
          
    public Person getPerson(int i){
        return memberReport.get(i);
    }
    
    
    @Override
    public int getRowCount() {
        return memberReport.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Person member=memberReport.get(i);
        switch(i1){
       
            case 0:
                return member.id;
                
            case 1:
                return member.name;
                
            case 2:
                return member.address;
                
            case 3:
                 return member.mail;
                 
            case 4:
                return member.contact;
      
            case 5:
                return member.gender;
        }
        
        return "";
    }
    
}
