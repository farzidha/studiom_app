/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.awt.Button;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.TableModel;

public class ExpenseReport extends javax.swing.JInternalFrame{

    JDesktopPane desktop;
    /**
     * Creates new form ExpenseReport
     */
    public ExpenseReport() {    
        
        setResizable(true);
        setTitle("Expense Report");
        initComponents();
            jXDatePicker1.setFormats(new SimpleDateFormat("dd/MM/yyyy"));
            jXDatePicker2.setFormats(new SimpleDateFormat("dd/MM/yyyy"));
            jXDatePicker3.setFormats(new SimpleDateFormat("dd/MM/yyyy"));
         
            expenseReport.addMouseListener(new MouseAdapter() {
            
                @Override
                public void mouseClicked(MouseEvent me) {

//                    System.out.println("No of columns  "+expenseReport.getColumnModel().getColumnCount());

                    int count=expenseReport.getColumnModel().getColumnCount();

                    if(count==4){
                        if(expenseReport.getSelectedColumn()==3){
                            try {
                                Login();
                            } catch (SQLException ex) {
                                Logger.getLogger(ExpenseReport.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }                    
                    }else{
                        if(expenseReport.getSelectedColumn()==4){                     
                            try {
                                Login();

                            } catch (SQLException ex) {
                                Logger.getLogger(ExpenseReport.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } 
                    }                   
                }

                private void Login() throws SQLException {

                        JTextField username = new JTextField();
                        JTextField password = new JPasswordField();
                        Object[] message = {
                            "Username:", username,
                            "Password:", password
                         };
                  
                    int option = JOptionPane.showConfirmDialog(null, message, "Login", JOptionPane.OK_CANCEL_OPTION);
                    if (option == JOptionPane.OK_OPTION) {
     
                        String pass = null,user = null;
                            int id = 0;
                            Statement stmt=Settings.con.createStatement();
                            ResultSet rs=stmt.executeQuery("select * from login");
                            if(rs.next()){
                               user=rs.getString("user_name");
                               pass=rs.getString("password");
                               id=rs.getInt(1);                         
                            }  
                        
                            if(username.getText().equals(user) && password.getText().equals(pass)) {
    
                                JOptionPane.showInternalMessageDialog(ExpenseReport.this, "Login successfull","Success",  JOptionPane.INFORMATION_MESSAGE);                                   
//                                System.out.println("edit clicked");
                                Person p =new Person();
                                int row = expenseReport.getSelectedRow();
                                p =((ExpenseFilterTable)expenseReport.getModel()).getPerson(row);
//                                System.out.println("amount " +p.amount+", date "+p.date +", type "+p.type +", billing id "+p.id+" ,name = "+p.staff);
                                Expense expense=new Expense(p);
                                desktop.add(expense);
                                Dimension desktopSize =desktop.getSize();
                                Dimension jInternalFrameSize =expense.getSize();
                                expense.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
                                        (desktopSize.height - jInternalFrameSize.height)/2);
                                expense.setVisible(true);                                       

                            }else{

                                JOptionPane.showInternalMessageDialog(ExpenseReport.this,"Enter Valid Username and Password!", "Login Failed",  JOptionPane.ERROR_MESSAGE);
                            }
                    }else {

                        JOptionPane.showInternalMessageDialog(ExpenseReport.this,"Login cancelled", "Cancelled",  JOptionPane.CANCEL_OPTION);
                    }                
                }            
            });
            
            expenseReport.setDefaultRenderer(Button.class, new ExpenseReportRenderer());
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jXDatePicker1 = new org.jdesktop.swingx.JXDatePicker();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jXDatePicker2 = new org.jdesktop.swingx.JXDatePicker();
        jXDatePicker3 = new org.jdesktop.swingx.JXDatePicker();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        expenseReport = new javax.swing.JTable();
        label1 = new java.awt.Label();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setClosable(true);
        setMaximizable(true);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Expense Type");

        jComboBox1.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "All", "Electricity Bill", "Staff salary", "Telephone bill", "Travel allowance", "Internet Bill", "Others" }));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Expense on");

        jXDatePicker1.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Expense From :");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Expense To :");

        jXDatePicker2.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N

        jXDatePicker3.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N

        jButton1.setFont(new java.awt.Font("SansSerif", 0, 15)); // NOI18N
        jButton1.setText("Submit");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        expenseReport.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        expenseReport.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Expense Type", "Date", "Amount", "Staff"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        expenseReport.setRowHeight(20);
        expenseReport.setRowMargin(0);
        jScrollPane1.setViewportView(expenseReport);

        label1.setAlignment(java.awt.Label.CENTER);
        label1.setBackground(new java.awt.Color(244, 144, 22));
        label1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        label1.setForeground(new java.awt.Color(255, 255, 255));
        label1.setText("EXPENSE REPORT");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Total Expense : ");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jButton2.setFont(new java.awt.Font("SansSerif", 0, 15)); // NOI18N
        jButton2.setText("Print");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("SansSerif", 0, 15)); // NOI18N
        jButton3.setText("Export");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(label1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 815, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(387, 387, 387))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3))
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jXDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jXDatePicker3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jXDatePicker1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton3)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jXDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jXDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jButton1)
                    .addComponent(jXDatePicker3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton3))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        String type=(String) jComboBox1.getSelectedItem();
        SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date expense_on=jXDatePicker1.getDate();
        Date from=jXDatePicker2.getDate();
        Date to=jXDatePicker3.getDate();
        int sum=0;
                  
         try{
           
                ArrayList<Person> expensefilter =new ArrayList<Person>();
                Statement stmt=Settings.con.createStatement();                
                String query="select * from expense";  

                 int conditions = 0;
                 if(!type.equals("All") && !type.equals("Others") && !type.equals("Staff salary")) {
                     
                    query += " where type ='" + type + "'"; 
                    conditions++;                                      
                    }
                
                 if(type.equals("Others")){
                    query+= " where type not  in ('Electricity Bill','Staff salary','Telephone bill','Travel allowance','Internet Bill')";
                    conditions++;
                 
                 }
                 if(type.equals("Staff salary")){
                    query+=",staff where expense.id=staff.exp_id and expense.type='"+type+"'";
                    conditions++;
                 }
                
                 if(expense_on!=null && from==null && to==null) {  
                     
                    query +=  (conditions > 0 ? " and " : " where ") + " paid_date='" + toFormat.format(expense_on) + "'";
                    conditions++;
                    }
                 if(expense_on==null && from!=null && to!=null){
                     
                     query +=(conditions > 0 ? " and " : " where ") + " paid_date between '" + toFormat.format(from) + "' and '" + toFormat.format(to) + "'";
                     conditions++;
                    }
                 
                   ResultSet rs= stmt.executeQuery(query);
                   
                    while(rs.next()){
                        Person expense=new Person();
                        expense.id=rs.getInt(1);
                        expense.type=rs.getString(2);
                        expense.date=rs.getDate(3);
                        expense.amount=rs.getInt(4);
                        expense.staff=rs.getString(5);
                        sum=sum+rs.getInt(4);
                        expensefilter.add(expense);                                           
                    }                    
                    ExpenseFilterTable eft=new ExpenseFilterTable(expensefilter);
                    expenseReport.setModel(eft);                   
                    jLabel6.setText(Integer.toString(sum));
        }catch(Exception ex){
            
            Logger.getLogger(BillingReport.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        
        MessageFormat header=new MessageFormat("Expense Report");
        MessageFormat footer=new MessageFormat("Page");
        try {
            // TODO add your handling code here:      
               expenseReport.print(JTable.PrintMode.FIT_WIDTH,header,footer);                 
               
        }catch (PrinterException ex) {
                Logger.getLogger(ExpenseReport.class.getName()).log(Level.SEVERE, null, ex);
                
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
       FileNameExtensionFilter filter = new FileNameExtensionFilter("EXCEL", "xlsx", "excel");
        jfc.setFileFilter(filter);
        int returnValue = jfc.showOpenDialog(null);
        // int returnValue = jfc.showSaveDialog(null);
        try {
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = jfc.getSelectedFile();
                    if(selectedFile.exists() || new File(selectedFile.getAbsolutePath() + ".xlsx").exists()) {
                        int result = JOptionPane.showConfirmDialog(this,"The file exists, overwrite?","Existing file",JOptionPane.YES_NO_CANCEL_OPTION);
                        if(result == JOptionPane.YES_OPTION) {
                             exportTable(expenseReport, new File(selectedFile.getAbsolutePath()));
                             JOptionPane.showInternalMessageDialog(this, "Exported successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
                        }
                    } else {
                        exportTable(expenseReport, new File(selectedFile.getAbsolutePath() + ".xlsx"));
                        JOptionPane.showInternalMessageDialog(this, "Exported successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
                    }
            }
        } catch (IOException ex) {
               Logger.getLogger(MemberReport.class.getName()).log(Level.SEVERE, null, ex);
           }
    }//GEN-LAST:event_jButton3ActionPerformed

    public void exportTable(JTable table, File file) throws IOException {
            TableModel model = table.getModel();
            FileWriter out = new FileWriter(file);
            for(int i=0; i < model.getColumnCount(); i++) {
                out.write(model.getColumnName(i) + "\t");
            }
            out.write("\n");
            for(int i=0; i< model.getRowCount(); i++) {
                for(int j=0; j < model.getColumnCount(); j++) {
                    String str = model.getValueAt(i,j) == null ? "" : model.getValueAt(i,j).toString();
                    String splitted[] = str.split(",");
                    StringBuffer sb = new StringBuffer();
                    String retrieveData = "";
                    for(int k =0; k<splitted.length; k++){
                        retrieveData = splitted[k];
                        if((retrieveData.trim()).length()>0){
                            if(k!=0){
                                sb.append("");
                            }
                            sb.append(retrieveData);

                        }
                    }

                str = sb.toString();
                out.write(str+"\t");
                }
            out.write("\n");
            }

            out.close();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable expenseReport;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker1;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker2;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker3;
    private java.awt.Label label1;
    // End of variables declaration//GEN-END:variables
  
}
