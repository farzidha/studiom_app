/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author bluroe
 */
public class BillingTableModel extends AbstractTableModel{

  
    private static final String[] columnNames={"Invoice No", "Name","Phone","Date","Sub Total","Discount","Tax", "Grand Total","Payment Mode", "","",""};
    private static final Class [] columnClasses={Integer.class,String.class,String.class,String.class,Integer.class,Float.class,Float.class,Float.class,String.class,JButton.class,JButton.class,JButton.class};
    private ArrayList<Person> billingList;
    
    public void removeRow(int rowIndex) {
        billingList.remove(rowIndex);
        fireTableRowsDeleted(rowIndex,rowIndex);
    }
    
    public BillingTableModel(ArrayList<Person> persons) {
      this.billingList = persons;
    }
    
    public String getColumnName(int col){
        return columnNames[col];
    }
     
    public Class<?> getColumnClass(int col){
        return columnClasses[col];
    }
          
    public Person getPerson(int i){
        return billingList.get(i);
    }
    
    @Override
    public int getRowCount() {
        
        return billingList.size();
    }

    @Override
    public int getColumnCount() {
       return columnNames.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Person person=billingList.get(i);
         
        
        switch (i1){   
            case 0:
                return person.id;
            case 1:
                return person.name;
                
            case 2:
                return person.contact;
                
            case 3: 
                return person.bill_date;
               
            case 4:
                return person.sub_total;
                
            case 5:
                return person.discount;
                
            case 6:
                return person.tax;
                
            case 7:
                return person.total;
            
            case 8:
                return person.mode;
                
            case 9:
                return person;
            
            case 10:
                return person;
                
            case 11:
                return person;
        }
        return "";
    } 
}
