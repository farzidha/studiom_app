/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.awt.Button;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.TableModel;

/**
 *
 * @author bluroe
 */
public class BillingReport extends javax.swing.JInternalFrame {

    JDesktopPane desktop;
    private List<String> name_array;
    private List<String> service_array;
    private List<String> staff_array;
    private int currentCaretPosition=0;
    /**
     * Creates new form BillingReport
     */
    public BillingReport() {
        
         setResizable(true);
         
         setTitle("Billing Report");
         initComponents();
        
        filterNameCombo(populateMemberArray());
        filterServiceCombo(populateServiceArray());
        filterStaffCombo(populateStaffArray());
           jXDatePicker1.setFormats(new SimpleDateFormat("dd/MM/yyyy"));
           jXDatePicker2.setFormats(new SimpleDateFormat("dd/MM/yyyy"));
           jXDatePicker3.setFormats(new SimpleDateFormat("dd/MM/yyyy"));
         
             jTable1.addMouseListener(new MouseAdapter() {
            
                @Override
                public void mouseClicked(MouseEvent me) {

                    if(jTable1.getSelectedColumn()==5){
                        
                        try {                    
                            Login();
                            
                        } catch (SQLException ex) {
                            Logger.getLogger(BillingReport.class.getName()).log(Level.SEVERE, null, ex);
                        }                      
                    }                                         
                }                             
                private void Login() throws SQLException  {

                        JTextField username = new JTextField();
                        JTextField password = new JPasswordField();
                        Object[] message = {
                            "Username:", username,
                            "Password:", password
                         };

                        int option = JOptionPane.showConfirmDialog(null, message, "Login", JOptionPane.OK_CANCEL_OPTION);
                        if (option == JOptionPane.OK_OPTION) {
//                            System.out.println("login");
                            
                            String pass = null,user = null;
                            int id = 0;
     
//                            System.out.println("login2");
                            
                            Statement state=Settings.con.createStatement();
                            ResultSet rs=state.executeQuery("select * from login");
//                             System.out.println("login3");
                            if(rs.next()){
                             
                               user=rs.getString("user_name");
                               pass=rs.getString("password");
                               id=rs.getInt(1);                        
                            }  
                        
                            if(username.getText().equals(user) && password.getText().equals(pass)) {

                                JOptionPane.showInternalMessageDialog(BillingReport.this, "Login successfull","Success",  JOptionPane.INFORMATION_MESSAGE);                      
                                    try {
                                        
                                        Person p =new Person();
                                        int row = jTable1.getSelectedRow();
                                         p =((BillingFilterTable)jTable1.getModel()).getPerson(row);
                                         Statement st=Settings.con.createStatement();
//                                         String room = null;
                                         ResultSet set=st.executeQuery("select room from member_lodge,members where member_lodge.member_id=members.id and members.id=" +p.id);
                                            if(set.next()){
                                                p.room=set.getString(1);

                                            }                                 
                                        Billing bill=new Billing(p);
                                        desktop.add(bill);

                                        Dimension desktopSize = desktop.getSize();
                                        Dimension jInternalFrameSize = bill.getSize();
                                        bill.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
                                                (desktopSize.height- jInternalFrameSize.height)/2);                                             
                                        bill.setVisible(true);
                                    } catch (SQLException ex) {

                                        Logger.getLogger(BillingReport.class.getName()).log(Level.SEVERE, null, ex);
                                    }

                            } else {

                                JOptionPane.showInternalMessageDialog(BillingReport.this,"Enter Valid Username and Password!", "Login Failed",  JOptionPane.ERROR_MESSAGE);
                            }
                        } else {

                               JOptionPane.showInternalMessageDialog(BillingReport.this,"Login cancelled", "Cancelled",  JOptionPane.CANCEL_OPTION);
                        }
                    }                              
            });
            
                jTable1.setDefaultRenderer(Button.class, new BillingReportRenderer());                             
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        serviceCombo = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jXDatePicker1 = new org.jdesktop.swingx.JXDatePicker();
        jLabel3 = new javax.swing.JLabel();
        jXDatePicker2 = new org.jdesktop.swingx.JXDatePicker();
        jLabel4 = new javax.swing.JLabel();
        jXDatePicker3 = new org.jdesktop.swingx.JXDatePicker();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        label1 = new java.awt.Label();
        jLabel5 = new javax.swing.JLabel();
        totalText = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        staffCombo = new javax.swing.JComboBox<>();
        nameCombo = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        taxText = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        modeCombo = new javax.swing.JComboBox<>();
        jButton3 = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setClosable(true);
        setMaximizable(true);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Service");

        serviceCombo.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Billing  on :");

        jXDatePicker1.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Billing  From :");

        jXDatePicker2.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("To :");

        jXDatePicker3.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N

        jButton1.setFont(new java.awt.Font("SansSerif", 0, 15)); // NOI18N
        jButton1.setText("Submit");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTable1.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Invoice No", "Customer", "Service", "Date", "Staff", "Amount", "Payment Mode"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setGridColor(new java.awt.Color(204, 204, 204));
        jTable1.setRowHeight(22);
        jScrollPane1.setViewportView(jTable1);

        label1.setAlignment(java.awt.Label.CENTER);
        label1.setBackground(new java.awt.Color(244, 144, 22));
        label1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        label1.setForeground(new java.awt.Color(255, 255, 255));
        label1.setText("BILLING REPORT");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Total Amount : ");

        totalText.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        totalText.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jButton2.setFont(new java.awt.Font("SansSerif", 0, 15)); // NOI18N
        jButton2.setText("Print");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("Staff");

        staffCombo.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N

        nameCombo.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Customer");

        jLabel6.setText("Total Tax :");

        taxText.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        taxText.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Payment Mode");

        modeCombo.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        modeCombo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "All", "Cash", "Card" }));

        jButton3.setFont(new java.awt.Font("SansSerif", 0, 15)); // NOI18N
        jButton3.setText("Export");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(label1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(serviceCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(nameCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jXDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 156, Short.MAX_VALUE)
                    .addComponent(staffCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jXDatePicker3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jXDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(modeCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3)))
                .addContainerGap(26, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(totalText, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(taxText, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(358, 358, 358))
            .addComponent(jScrollPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(serviceCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jXDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jXDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(modeCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jXDatePicker3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jLabel8)
                    .addComponent(staffCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nameCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jButton3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(totalText, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(taxText, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");
        String service=serviceCombo.getSelectedItem() == null ? "" :(String) serviceCombo.getSelectedItem();
        String str= nameCombo.getSelectedItem() == null ? "" :(String) nameCombo.getSelectedItem();
        String customer = "";
        String phone = "";
        String mode=(String) modeCombo.getSelectedItem();
        int index = -1;
        if(nameCombo.getSelectedIndex() != -1 || !str.equals("")) {
            index = str.indexOf("~");
            customer = str;
            phone = str;
        }
        if(index != -1)
        {
            String[] temp;
            String sep= "~";
            temp = str.split(sep);
            customer = temp[0];
            phone = temp[1];
        } 
        String staff=staffCombo.getSelectedItem() == null ? "" :(String) staffCombo.getSelectedItem();
        Date bill_on=jXDatePicker1.getDate();
        Date from=jXDatePicker2.getDate();
        Date to=jXDatePicker3.getDate();
        int sum=0;
        float taxSum = 0;      
            try{
                ArrayList<Person> billingfilter =new ArrayList<Person>();
                Statement stmt=Settings.con.createStatement();
                
                String query="select distinct(b.id), b.mem_name, b.mem_phone, b.billing_date, b.sub_total, b.discount, b.tax, b.total, b.pay_mode from billing b,bill_items bi where b.id=bi.bill_id";
                if(serviceCombo.getSelectedIndex() != -1 && !service.equals("")) {
                     
                    query += " and bi.service ='"+service+"'";  
                    
                  }
                if(nameCombo.getSelectedIndex() != -1 && !str.equals("")) {
                     
                    query += " and (b.mem_name ='"+customer+"' or b.mem_phone ='"+phone+"')";  
                    
                  }
                 if(staffCombo.getSelectedIndex() != -1 && !staff.equals("")) {
                     
                    query += " and bi.staff ='"+staff+"'";  
                    
                  }
                 if(bill_on!=null && from==null && to==null) {    
                     
                    query +=  " and STR_TO_DATE(b.billing_date, '%d/%m/%Y')='" + toFormat.format(bill_on) + "'";
               
                 }
                 
                 if(bill_on==null && from!=null && to!=null){
                     
                     query +=" and (STR_TO_DATE(b.billing_date, '%d/%m/%Y') between '" + toFormat.format(from) + "' and '" + toFormat.format(to) + "')";
                 }
                 if(!mode.equals("All")) {
                     query +=" and b.pay_mode='"+mode+"' ";
                 }
                 ResultSet rs= stmt.executeQuery(query);
                   
                    while(rs.next()){
                        Person bill=new Person();
                        bill.id=rs.getInt("id");
                        bill.name=rs.getString("mem_name");
                        bill.contact=rs.getString("mem_phone");
                        bill.bill_date=rs.getString("billing_date");
                        bill.sub_total=rs.getInt("sub_total");
                        bill.discount=rs.getFloat("discount");
                        bill.tax=rs.getFloat("tax");
                        bill.total=rs.getFloat("total");
                        bill.mode=rs.getString("pay_mode");
//                        System.out.println("name " +rs.getString(8));
                        sum=sum+rs.getInt(8);
                        taxSum = taxSum + rs.getFloat(7);
                        billingfilter.add(bill);                    
                    }
                     BillingFilterTable bft=new BillingFilterTable(billingfilter);
                     jTable1.setModel(bft);
                     totalText.setText(Integer.toString(sum));
                     taxText.setText(Float.toString(taxSum));
                        
        }catch(Exception ex){
             Logger.getLogger(BillingReport.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            // TODO add your handling code here:
            MessageFormat header=new MessageFormat("Billing Report");
            jTable1.print(JTable.PrintMode.FIT_WIDTH,header,null);
        } catch (PrinterException ex) {
            Logger.getLogger(BillingReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        FileNameExtensionFilter filter = new FileNameExtensionFilter("EXCEL", "xlsx", "excel");
        jfc.setFileFilter(filter);
        int returnValue = jfc.showOpenDialog(null);
        // int returnValue = jfc.showSaveDialog(null);
        try {
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = jfc.getSelectedFile();
                    if(selectedFile.exists() || new File(selectedFile.getAbsolutePath() + ".xlsx").exists()) {
                        int result = JOptionPane.showConfirmDialog(this,"The file exists, overwrite?","Existing file",JOptionPane.YES_NO_CANCEL_OPTION);
                        if(result == JOptionPane.YES_OPTION) {
                             exportTable(jTable1, new File(selectedFile.getAbsolutePath()));
                             JOptionPane.showInternalMessageDialog(this, "Exported successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
                        }
                    } else {
                        exportTable(jTable1, new File(selectedFile.getAbsolutePath() + ".xlsx"));
                        JOptionPane.showInternalMessageDialog(this, "Exported successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
                    }
            }
        } catch (IOException ex) {
           Logger.getLogger(MemberReport.class.getName()).log(Level.SEVERE, null, ex);
       }
    }//GEN-LAST:event_jButton3ActionPerformed

    public void exportTable(JTable table, File file) throws IOException {
            TableModel model = table.getModel();
            FileWriter out = new FileWriter(file);
            for(int i=0; i < model.getColumnCount(); i++) {
                out.write(model.getColumnName(i) + "\t");
            }
            out.write("\n");

            for(int i=0; i< model.getRowCount(); i++) {
                for(int j=0; j < model.getColumnCount(); j++) {
                    String str = model.getValueAt(i,j).toString();
                    String splitted[] = str.split(",");
                    StringBuffer sb = new StringBuffer();
                    String retrieveData = "";
                    for(int k =0; k<splitted.length; k++){
                        retrieveData = splitted[k];
                        if((retrieveData.trim()).length()>0){
                            if(k!=0){
                                sb.append("");
                            }
                            sb.append(retrieveData);

                        }
                    }

                str = sb.toString();
                out.write(str+"\t");
                }
            out.write("\n");
            }

            out.close();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker1;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker2;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker3;
    private java.awt.Label label1;
    private javax.swing.JComboBox<String> modeCombo;
    private javax.swing.JComboBox<String> nameCombo;
    private javax.swing.JComboBox<String> serviceCombo;
    private javax.swing.JComboBox<String> staffCombo;
    private javax.swing.JLabel taxText;
    private javax.swing.JLabel totalText;
    // End of variables declaration//GEN-END:variables
    public static List<String> populateMemberArray() {
        List<String> customer = new ArrayList<String>();
        try {
            Statement stmt = Settings.con.createStatement();
            ResultSet rs=stmt.executeQuery("select name, contact from members order by name asc");
            customer.add("");
            while(rs.next()){
                 customer.add(rs.getString(1) + "~" + rs.getString(2));
            }
        } catch(SQLException e) {
            System.out.println(e);
        }
        return customer;
    }
    
    public static List<String> populateServiceArray() {
        List<String> service = new ArrayList<String>();
        try {
            Statement stmt = Settings.con.createStatement();
            ResultSet rs=stmt.executeQuery("select name from services order by name asc");
            service.add("");
            while(rs.next()){
                 service.add(rs.getString(1));
            }
        } catch(SQLException e) {
            System.out.println(e);
        }
        return service;
    }

    public static List<String> populateStaffArray() {
        List<String> staff = new ArrayList<String>();
        try {
            Statement stmt = Settings.con.createStatement();
            ResultSet rs=stmt.executeQuery("select name from staff order by name asc");
            staff.add("");
            while(rs.next()){
                 staff.add(rs.getString(1));
            }
        } catch(SQLException e) {
            System.out.println(e);
        }
        return staff;
    }
    
    private void filterNameCombo(List<String> array) {
        this.name_array = array;
        nameCombo.setEditable(true);
        final JTextField textfield = (JTextField) nameCombo.getEditor().getEditorComponent();
        textfield.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent ke) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        currentCaretPosition=textfield.getCaretPosition();
                        if(textfield.getSelectedText()==null)
                        {
                        textfield.setCaretPosition(0);
                        comboNameFilter(textfield.getText());
                        textfield.setCaretPosition(currentCaretPosition);
                        }
                    }
                });
            }
        });
        textfield.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        comboNameFilter("");
                    }
                });
            }
        }); 
    }
    
    public void comboNameFilter(String enteredText) {
        List<String> filterArray= new ArrayList<String>();
        for (int i = 0; i < name_array.size(); i++) {
            if (name_array.get(i).toLowerCase().contains(enteredText.toLowerCase())) {
                filterArray.add(name_array.get(i));
            }


        }
        if (filterArray.size() > 0) {

            nameCombo.setModel(new DefaultComboBoxModel(filterArray.toArray()));
            nameCombo.setSelectedItem(enteredText);
            nameCombo.showPopup();
        }
        else {
            nameCombo.hidePopup();
        }
    }
    
    private void filterServiceCombo(List<String> array) {
        this.service_array = array;
        serviceCombo.setEditable(true);
        final JTextField textfield = (JTextField) serviceCombo.getEditor().getEditorComponent();
        textfield.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent ke) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        currentCaretPosition=textfield.getCaretPosition();
                        if(textfield.getSelectedText()==null)
                        {
                        textfield.setCaretPosition(0);
                        comboServiceFilter(textfield.getText());
                        textfield.setCaretPosition(currentCaretPosition);
                        }
                    }
                });
            }
        });
        textfield.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        comboServiceFilter("");
                    }
                });
            }
        }); 
    }
    
    public void comboServiceFilter(String enteredText) {
        List<String> filterArray= new ArrayList<String>();
        for (int i = 0; i < service_array.size(); i++) {
            if (service_array.get(i).toLowerCase().contains(enteredText.toLowerCase())) {
                filterArray.add(service_array.get(i));
            }


        }
        if (filterArray.size() > 0) {

            serviceCombo.setModel(new DefaultComboBoxModel(filterArray.toArray()));
            serviceCombo.setSelectedItem(enteredText);
            serviceCombo.showPopup();
        }
        else {
            serviceCombo.hidePopup();
        }
    }
    
    private void filterStaffCombo(List<String> array) {
        this.staff_array = array;
        staffCombo.setEditable(true);
        final JTextField textfield = (JTextField) staffCombo.getEditor().getEditorComponent();
        textfield.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent ke) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        currentCaretPosition=textfield.getCaretPosition();
                        if(textfield.getSelectedText()==null)
                        {
                        textfield.setCaretPosition(0);
                        comboStaffFilter(textfield.getText());
                        textfield.setCaretPosition(currentCaretPosition);
                        }
                    }
                });
            }
        });
        textfield.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        comboStaffFilter("");
                    }
                });
            }
        }); 
    }
    
    public void comboStaffFilter(String enteredText) {
        List<String> filterArray= new ArrayList<String>();
        for (int i = 0; i < staff_array.size(); i++) {
            if (staff_array.get(i).toLowerCase().contains(enteredText.toLowerCase())) {
                filterArray.add(staff_array.get(i));
            }


        }
        if (filterArray.size() > 0) {

            staffCombo.setModel(new DefaultComboBoxModel(filterArray.toArray()));
            staffCombo.setSelectedItem(enteredText);
            staffCombo.showPopup();
        }
        else {
            staffCombo.hidePopup();
        }
    }
}
