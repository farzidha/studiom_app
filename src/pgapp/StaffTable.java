/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.SwingWorker;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author bluroe
 */
public class StaffTable extends javax.swing.JInternalFrame {

    /**
     * Creates new form MemberTable
     */
    ArrayList<Person> persons=new ArrayList<Person>();
    public StaffTable() throws SQLException, IOException {
        
        setTitle("STAFF LIST");
        setResizable(true);
        initComponents();
        
        staffTable.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseClicked(MouseEvent me) {
                if(staffTable.getSelectedColumn()==5){
                   int row=staffTable.getSelectedRow();
                   if (staffTable.getRowSorter()!=null) {
                        row = staffTable.getRowSorter().convertRowIndexToModel(row);
                   }
                   Person p=((StaffTableModel)staffTable.getModel()).getPerson(row);
                   
                   try {
                       JOptionPane waiting=new JOptionPane("Opening please wait", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION,null,new Object[]{},null);
                       JDialog waitingdlg=waiting.createDialog(StaffTable.this,"Waiting");
                       waitingdlg.setModal(false);
                       waitingdlg.setVisible(true);
                       SwingWorker sw=new SwingWorker() {
                           @Override
                           protected Object doInBackground() throws Exception {
                                StaffForm edit = new StaffForm(p);
                                desktop.add(edit);
                                edit.setVisible(true);
                                waitingdlg.dispose();
                                return null;
                           }
                       };
                       sw.execute();
                   } catch (Exception ex) {
                       Logger.getLogger(StaffTable.class.getName()).log(Level.SEVERE, null, ex);
                   }
               } else if( staffTable.getSelectedColumn()==6){
                       int row = staffTable.getSelectedRow();
                       if (staffTable.getRowSorter()!=null) {
                            row = staffTable.getRowSorter().convertRowIndexToModel(row);
                       }
                       Person p =((StaffTableModel)staffTable.getModel()).getPerson(row);
                       try {
                           int id=p.id;                           
                           Statement stmt=Settings.con.createStatement();
                           stmt.executeUpdate("delete from staff where id=" + id);
                           ((StaffTableModel)staffTable.getModel()).removeRow(row);
                           staffTable.clearSelection();
                        } catch (SQLException ex) {
                           
                           Logger.getLogger(StaffTable.class.getName()).log(Level.SEVERE, null, ex);
                        }
                       
                }
            }         
        });
        staffTable.setDefaultRenderer(JButton.class, new StaffTableCellRenderer());
        
        reloadData();
        StaffTableModel mtm=new StaffTableModel(persons);
        sorter = new TableRowSorter<StaffTableModel>(mtm);
        staffTable.setModel(mtm);
        staffTable.setRowSorter(sorter);
    }
    
    private void reloadData() {
        try {
            Statement stmt = Settings.con.createStatement();
            ResultSet rs=stmt.executeQuery("select id,name,address,contact,mail,gender from staff order by name asc");

            while(rs.next()){

                 Person p=new Person();
                 p.id=rs.getInt(1);
                 p.name=rs.getString(2);
                 p.address=rs.getString(3);
                 p.contact=rs.getString(4);
                 p.mail=rs.getString(5);
                 p.gender=rs.getString(6);     
                 persons.add(p);

            }
        } catch(SQLException ex) {
            Logger.getLogger(StaffTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void newFilter() {
    
    RowFilter<StaffTableModel, Object> rf = null;
    // If current expression doesn't parse, don't update.
    try {
      rf = RowFilter.regexFilter("(?i)" + searchText.getText(), 0);
    } catch (java.util.regex.PatternSyntaxException e) {
      return;
    }
    sorter.setRowFilter(rf);
  }
   

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        staffTable = new javax.swing.JTable();
        label1 = new java.awt.Label();
        jLabel1 = new javax.swing.JLabel();
        searchText = new javax.swing.JTextField();

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        jButton1.setText("jButton1");

        setBackground(new java.awt.Color(255, 255, 255));
        setClosable(true);
        setMaximizable(true);

        staffTable.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        staffTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id", "Name", "Guardian", "Address", "Proof type", "proof", "Contact", "Rent", "Room", "Action"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        staffTable.setGridColor(new java.awt.Color(204, 204, 204));
        staffTable.setRowHeight(24);
        jScrollPane1.setViewportView(staffTable);

        label1.setAlignment(java.awt.Label.CENTER);
        label1.setBackground(new java.awt.Color(244, 144, 22));
        label1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        label1.setForeground(new java.awt.Color(255, 255, 255));
        label1.setText("STAFF LIST");

        jLabel1.setText("Search");

        searchText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                searchTextKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(label1, javax.swing.GroupLayout.DEFAULT_SIZE, 990, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addGap(4, 4, 4)
                        .addComponent(searchText, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(searchText, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 447, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void searchTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchTextKeyTyped
        newFilter();
    }//GEN-LAST:event_searchTextKeyTyped

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private java.awt.Label label1;
    private javax.swing.JTextField searchText;
    private javax.swing.JTable staffTable;
    // End of variables declaration//GEN-END:variables
    javax.swing.JDesktopPane desktop;
    private TableRowSorter<StaffTableModel> sorter;
    
    

}
