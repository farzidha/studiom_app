/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.awt.Button;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author bluroe
 */
public class ExpenseFilterTable extends AbstractTableModel{
    
    
        private static final String[] columnNames={"Expense Type","Date","Amount","Staff Name"};
        private static final Class [] columnClasses={String.class,String.class,int.class,String.class};
        private ArrayList<Person> expense;
        
        ExpenseFilterTable(ArrayList<Person> expensefilter) {
            this.expense=expensefilter;
        }
        
         public String getColumnName(int col){
            return  columnNames[col];
        }
    
        public Class<?> getColumnClass(int col){
            return columnClasses[col];
        }
          
        public Person getPerson(int i){
            return expense.get(i);
        }

    @Override
    public int getRowCount() {
        return expense.size();
    }

    @Override
    public int getColumnCount() {
       return columnNames.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
       Person exp=expense.get(i);
       switch(i1){
           case 0:
               return  exp.type;
               
           case 1:
               return exp.date;
               
           case 2:
               return exp.amount;
              
           case 3:
               return exp.staff;
                             
       }
       return "";
    }
}
