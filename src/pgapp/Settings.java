package pgapp;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bluroe
 */
public class Settings {
    
    private final static String filename = "config.properties";
    public static String HOST;
    public static String PORT;
    public static String DBAS;
    public static String USER;
    public static String PASS;
    public static String KAP_USER;
    public static String KAP_PASS;
    public static String KAP_SENDER;
    public static Connection con;
    
    
    
    public static void loadSettings() {
        Properties props = new Properties();
        try {
            InputStream stream = new FileInputStream(filename);
            if(stream == null) {
                System.out.println("Sorry unable to find config file");
                System.exit(1);
            }
            props.load(stream);
        } catch(Exception e) {
            JOptionPane.showMessageDialog(null, "Config file not found", "Error", JOptionPane.ERROR_MESSAGE);
        }
        HOST = props.getProperty("DB_HOST", "localhost");
        PORT = props.getProperty("DB_PORT", "3306");
        DBAS = props.getProperty("DB_DBAS", "studiom_db");
        USER = props.getProperty("DB_USER", "root");
        PASS = props.getProperty("DB_PASS", "password");
        KAP_USER = props.getProperty("KAP_USER");
        KAP_PASS = props.getProperty("KAP_PASS");
        KAP_SENDER = props.getProperty("KAP_SENDER");
    }
    
    public static String getString() {
        return "HOST : " + HOST + ", PORT : " + PORT + ", DBAS : " + DBAS + ", USER : " + USER + ", PASS : " + PASS;
    }
    
}
