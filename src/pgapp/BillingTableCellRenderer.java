/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author bluroe
 */
public class BillingTableCellRenderer implements TableCellRenderer{
    
    @Override
    public Component getTableCellRendererComponent(JTable jtable, Object o, boolean bln, boolean bln1, int row, int col) {
        
        if(col==9){            
            return new JButton("EDIT");   
            
        }
        if(col==10){            
            return new JButton("DELETE");   
            
        }
        if(col==11){            
            return new JButton("PRINT");   
            
        }
        else{
            return null;
        }
    }  
    
}