/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

/**
 *
 * @author bluroe
 */
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class GridBagLayoutDemo extends JInternalFrame{
     
    
   
    
    final static boolean shouldFill = true;
    final static boolean shouldWeightX = true;
    final static boolean RIGHT_TO_LEFT = false;
    
    public void addComponentsToPane(Container pane) throws SQLException {
        
        HashMap<String, Integer> hmap = new HashMap<>();
        
        if (RIGHT_TO_LEFT) {
            pane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        }
        
        Statement stmt = Settings.con.createStatement();
        
        ResultSet r=stmt.executeQuery("select count(room),status,room from member_lodge where status='active' group by room having(count(room)>0);");
        while(r.next()){

            int count=r.getInt(1);
            String room= r.getString("room");
//            String pre=r.getString("prefix");
//            System.out.println(room +" count "+ count);

//            HashMap<Integer, Integer> hmap = new HashMap<Integer, Integer>();

            hmap.put(room,count);
        }
        System.out.println(hmap);
        
        ResultSet rs = stmt.executeQuery("select * from floors");

        pane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        
        int floory = 0;
        while(rs.next()) {
            int start = rs.getInt("from_no");
            int end = rs.getInt("to_no");
            final String prefix = rs.getString("prefix");
            String floor=rs.getString("name");
         
//            System.out.println("floor " + y);
            JPanel panel = new JPanel(new GridBagLayout());
            panel.setBorder(BorderFactory.createLineBorder(Color.black, 1));
            for(int i = start; i <= end; i++) {
//                System.out.println(prefix+i);
            final int number=i;
                JLabel label = new JLabel(prefix + i);
                label.setPreferredSize(new Dimension(80, 40));
                label.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        super.mouseClicked(e); 
//                        System.out.println(prefix+number);
                        String room_no=(prefix+number);
                       
                            
                        try {
                           DueLlist list = new DueLlist(room_no);
                           frame.getDesktop().add(list);
                           list.setVisible(true);
                        } catch (SQLException ex) {
                            Logger.getLogger(GridBagLayoutDemo.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                    }         
        });
                  
                int count = hmap.getOrDefault(prefix + i, 0);
                label.setOpaque(true);
                if(count == 0){

//                       button.setBackground(new Color(102, 255, 102)); 
                    label.setBackground(new Color(0, 255, 0));
                }

                else if(count == 1){

                    label.setBackground(new Color(255, 102, 255));
                } 
                else if(count >= 2){

                     label.setBackground(new Color(255, 51, 0));
                }
                c.gridx = (i - start) % 4;
                c.gridy = (i - start) / 4;
//                System.out.println("GRID " + c.gridx + ":" + c.gridy);
                c.insets = new Insets(5,5,5,5);  //top padding
                panel.add(label, c);
            }
            c.gridx = 0;
            c.gridy = floory;
            pane.add(new JLabel(floor), c);
            c.gridx = 1;
            c.gridy = floory++;
            pane.add(panel, c);

        }
//        System.out.println(floory);

    }           
    private final NewJFrame frame;
       /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public GridBagLayoutDemo(NewJFrame frame) throws SQLException {
        //Create and set up the window.
        this.frame = frame;
        JPanel jpanel=new JPanel();
        addComponentsToPane(jpanel);
//        setSize(600, 400);
        setSize(600, 600);


        getContentPane().add(new JScrollPane(jpanel));  
        setTitle("ROOMS AND FLOORS");
        setClosable(true);
        setResizable(true);
       
    }  
}
