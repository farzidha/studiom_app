/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author bluroe
 */
public class ServicesTableCellRenderer implements TableCellRenderer{
    
    @Override
    public Component getTableCellRendererComponent(JTable jtable, Object o, boolean bln, boolean bln1, int row, int col) {
        
        Person person=(Person) o;
        
        if(col==2){            
            return new JButton("EDIT");   
            
        }
        else if(col==3){            
            return new JButton("DELETE");   
            
        }
        else{
            return null;
        }
    }  
    
}