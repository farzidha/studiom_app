/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.SwingWorker;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author bluroe
 */
public class BillingTable extends javax.swing.JInternalFrame {

    /**
     * Creates new form MemberTable
     */
    ArrayList<Person> persons=new ArrayList<Person>();
    public BillingTable() throws SQLException, IOException {
        setTitle("INVOICE LIST");
        setResizable(true);
        initComponents();
        
        billingTable.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseClicked(MouseEvent me) {
                if(billingTable.getSelectedColumn()==9){
                   int row=billingTable.getSelectedRow();
                   if (billingTable.getRowSorter()!=null) {
                        row = billingTable.getRowSorter().convertRowIndexToModel(row);
                   }
                   Person p=((BillingTableModel)billingTable.getModel()).getPerson(row);
                   
                   try {
                       JOptionPane waiting=new JOptionPane("Opening please wait", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION,null,new Object[]{},null);
                       JDialog waitingdlg=waiting.createDialog(BillingTable.this,"Waiting");
                       waitingdlg.setModal(false);
                       waitingdlg.setVisible(true);
                       SwingWorker sw=new SwingWorker() {
                           @Override
                           protected Object doInBackground() throws Exception {
                                Billing edit = new Billing(p);
                                desktop.add(edit);
                                edit.setVisible(true);
                                waitingdlg.dispose();
                                return null;
                           }
                       };
                       sw.execute();
                   } catch (Exception ex) {
                       Logger.getLogger(BillingTable.class.getName()).log(Level.SEVERE, null, ex);
                   }
               } else if( billingTable.getSelectedColumn()==10){
                    int dialogButton = JOptionPane.YES_NO_OPTION;
                    int dialogResult = JOptionPane.showConfirmDialog (null, "Do You want to delete the invoice?","Warning",dialogButton);
                    if(dialogResult == JOptionPane.YES_OPTION){
                       int row = billingTable.getSelectedRow();
                       if (billingTable.getRowSorter()!=null) {
                            row = billingTable.getRowSorter().convertRowIndexToModel(row);
                       }
                       Person p =((BillingTableModel)billingTable.getModel()).getPerson(row);
                       try {
                           int id=p.id;                           
                           Statement stmt=Settings.con.createStatement();
                           stmt.executeUpdate("delete from bill_items where bill_id=" + id);
                           stmt.executeUpdate("delete from billing where id=" + id);
                           ((BillingTableModel)billingTable.getModel()).removeRow(row);
                           billingTable.clearSelection();
                        } catch (SQLException ex) {
                           
                           Logger.getLogger(BillingTable.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } 
                }
                else if( billingTable.getSelectedColumn()==11){
                    int row = billingTable.getSelectedRow();
                    Person p =((BillingTableModel)billingTable.getModel()).getPerson(row);
                    ArrayList<Person> customerList = new ArrayList<>();
                    try {
                        Statement stmt = Settings.con.createStatement();
                        ResultSet rs=stmt.executeQuery("select service, amount, qty, total from bill_items where bill_id ="+p.id);
                        while(rs.next()){
                            Person customer = new Person(rs.getString("service"), rs.getInt("amount"), rs.getInt("qty"),rs.getInt("total"));
                            customerList.add(customer);
                        }
                    } catch(SQLException e) {
                        System.out.println(e);
                    }
                    PageFormat format = new PageFormat();
                     Paper paper = new Paper();

                     double paperWidth = 3;//3.25
                     double paperHeight = 3.69;//11.69
                     double leftMargin = 1;
                     double rightMargin = 1;
                     double topMargin = 0;
                     double bottomMargin = 0.01;

                     paper.setSize(paperWidth * 200, paperHeight * 200);
                     paper.setImageableArea(leftMargin * 200, topMargin * 200,
                     (paperWidth - leftMargin - rightMargin) * 200,
                     (paperHeight - topMargin - bottomMargin) * 200);
                
                     format.setPaper(paper);

                     PrintRequestAttributeSet attr = new HashPrintRequestAttributeSet();
                     attr.add(OrientationRequested.PORTRAIT);
                     PrinterJob printerJob = PrinterJob.getPrinterJob();
     //                LocalDate from = new java.sql.Date(jXDatePicker1.getDate().getTime()).toLocalDate();
     //                LocalDate lastday=from.with(TemporalAdjusters.lastDayOfMonth());
                     Printable printable = new Receipt(p.name, p.contact, p.bill_date, p.sub_total, p.discount, p.tax, p.total,customerList);

//                     format = printerJob.validatePage(format);
                     if(printerJob.printDialog()) {
                         printerJob.setPrintable(printable, format);
                         try {
                             printerJob.print(attr);
                         } catch(Exception e) {}
                     }
                }
            }         
        });
        billingTable.setDefaultRenderer(JButton.class, new BillingTableCellRenderer());
        
        reloadData();
        BillingTableModel mtm=new BillingTableModel(persons);
        sorter = new TableRowSorter<BillingTableModel>(mtm);
        billingTable.setModel(mtm);
        billingTable.setRowSorter(sorter);
    }
    
    private void reloadData() {
        try {
            Statement stmt = Settings.con.createStatement();
            ResultSet rs=stmt.executeQuery("select id,mem_name,mem_phone,billing_date,sub_total,discount,tax,total,pay_mode from billing order by id desc");

            while(rs.next()){

                 Person p=new Person();
                 p.id=rs.getInt(1);
                 p.name=rs.getString(2);
                 p.contact=rs.getString(3);
                 p.bill_date=rs.getString(4);
                 p.sub_total=rs.getInt(5);
                 p.discount=rs.getFloat(6); 
                 p.tax=rs.getFloat(7);    
                 p.total=rs.getFloat(8);   
                 p.mode=rs.getString(9); 
                 persons.add(p);

            }
        } catch(SQLException ex) {
            Logger.getLogger(BillingTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void newFilter() {
    
    RowFilter<BillingTableModel, Object> rf = null;
    // If current expression doesn't parse, don't update.
    try {
      rf = RowFilter.regexFilter("(?i)" + searchText.getText(), 0);
    } catch (java.util.regex.PatternSyntaxException e) {
      return;
    }
    sorter.setRowFilter(rf);
  }
   

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        billingTable = new javax.swing.JTable();
        label1 = new java.awt.Label();
        jLabel1 = new javax.swing.JLabel();
        searchText = new javax.swing.JTextField();

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        jButton1.setText("jButton1");

        setBackground(new java.awt.Color(255, 255, 255));
        setClosable(true);
        setMaximizable(true);

        billingTable.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        billingTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id", "Name", "Guardian", "Address", "Proof type", "proof", "Contact", "Rent", "Room", "Action"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        billingTable.setGridColor(new java.awt.Color(204, 204, 204));
        billingTable.setRowHeight(24);
        jScrollPane1.setViewportView(billingTable);

        label1.setAlignment(java.awt.Label.CENTER);
        label1.setBackground(new java.awt.Color(244, 144, 22));
        label1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        label1.setForeground(new java.awt.Color(255, 255, 255));
        label1.setText("INVOICE LIST");

        jLabel1.setText("Search");

        searchText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                searchTextKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(label1, javax.swing.GroupLayout.DEFAULT_SIZE, 990, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addGap(4, 4, 4)
                        .addComponent(searchText, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(searchText, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 447, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void searchTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchTextKeyTyped
        newFilter();
    }//GEN-LAST:event_searchTextKeyTyped

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable billingTable;
    private javax.swing.JButton jButton1;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private java.awt.Label label1;
    private javax.swing.JTextField searchText;
    // End of variables declaration//GEN-END:variables
    javax.swing.JDesktopPane desktop;
    private TableRowSorter<BillingTableModel> sorter;
    
    

}
