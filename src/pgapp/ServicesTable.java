/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

/**
 *
 * @author bluroe
 */
public class ServicesTable extends javax.swing.JInternalFrame {

    /**
     * Creates new form MemberTable
     */
    public ServicesTable() throws SQLException, IOException {
        
        setTitle("SERVICES LIST");
        setResizable(true);
        initComponents();
        servicesTable.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseClicked(MouseEvent me) {
                if(servicesTable.getSelectedColumn()==2){
                   int row=servicesTable.getSelectedRow();
                   Person p=((ServicesTableModel)servicesTable.getModel()).getPerson(row);
                   
                   try {
                       JOptionPane waiting=new JOptionPane("Opening please wait", JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION,null,new Object[]{},null);
                       JDialog waitingdlg=waiting.createDialog(ServicesTable.this,"Waiting");
                       waitingdlg.setModal(false);
                       waitingdlg.setVisible(true);
                       SwingWorker sw=new SwingWorker() {
                           @Override
                           protected Object doInBackground() throws Exception {
                                ServicesForm edit = new ServicesForm(p);
                                desktop.add(edit);
                                edit.setVisible(true);
                                waitingdlg.dispose();
                                return null;
                           }
                       };
                       sw.execute();
                   } catch (Exception ex) {
                       Logger.getLogger(MemberTable.class.getName()).log(Level.SEVERE, null, ex);
                   }
               } else if( servicesTable.getSelectedColumn()==3){
                   int dialogButton = JOptionPane.YES_NO_OPTION;
                    int dialogResult = JOptionPane.showConfirmDialog (null, "Do You want to delete the service?","Warning",dialogButton);
                    if(dialogResult == JOptionPane.YES_OPTION){
                       int row = servicesTable.getSelectedRow();
                       Person p =((ServicesTableModel)servicesTable.getModel()).getPerson(row);
                       try {
                           int id=p.id;                           
                           Statement stmt=Settings.con.createStatement();
                           stmt.executeUpdate("delete from services where id=" + id);
                           ((ServicesTableModel)servicesTable.getModel()).removeRow(row);
                           servicesTable.clearSelection();
                        } catch (SQLException ex) {
                           
                           Logger.getLogger(MemberTable.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }         
        });
        servicesTable.setDefaultRenderer(JButton.class, new ServicesTableCellRenderer());
        
        ArrayList<Person> persons=new ArrayList<Person>();
        Statement stmt = Settings.con.createStatement();
       
        ResultSet rs=stmt.executeQuery("select id,name,price from services order by name asc");
        
        while(rs.next()){
             Person p=new Person();
             p.id=rs.getInt(1);
             p.name=rs.getString(2);
             p.price=rs.getFloat(3);
             persons.add(p);
        }
       
         ServicesTableModel mtm=new ServicesTableModel(persons);
         servicesTable.setModel(mtm);
    }
   

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        servicesTable = new javax.swing.JTable();
        label1 = new java.awt.Label();

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        jButton1.setText("jButton1");

        setBackground(new java.awt.Color(255, 255, 255));
        setClosable(true);
        setMaximizable(true);

        servicesTable.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        servicesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "id", "Name", "Guardian", "Address", "Proof type", "proof", "Contact", "Rent", "Room", "Action", "Edit"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        servicesTable.setGridColor(new java.awt.Color(204, 204, 204));
        servicesTable.setRowHeight(24);
        jScrollPane1.setViewportView(servicesTable);

        label1.setAlignment(java.awt.Label.CENTER);
        label1.setBackground(new java.awt.Color(244, 144, 22));
        label1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        label1.setForeground(new java.awt.Color(255, 255, 255));
        label1.setText("SERVICES LIST");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(label1, javax.swing.GroupLayout.DEFAULT_SIZE, 990, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JScrollPane jScrollPane1;
    private java.awt.Label label1;
    private javax.swing.JTable servicesTable;
    // End of variables declaration//GEN-END:variables
    javax.swing.JDesktopPane desktop;

}
