/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.awt.Button;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PrinterException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 *
 * @author bluroe
 */
public class SingleBillReport extends javax.swing.JInternalFrame {

    JDesktopPane desktop;
    private List<String> service_array;
    private List<String> staff_array;
    private int currentCaretPosition=0;
    /**
     * Creates new form BillingReport
     */
    public SingleBillReport() {
        
         setResizable(true);
         
         setTitle("Billing Report");
         initComponents();
        
        filterServiceCombo(populateServiceArray());
        filterStaffCombo(populateStaffArray());
         
             jTable1.addMouseListener(new MouseAdapter() {
            
                @Override
                public void mouseClicked(MouseEvent me) {

                    if(jTable1.getSelectedColumn()==5){
                        
                        try {                    
                            Login();
                            
                        } catch (SQLException ex) {
                            Logger.getLogger(SingleBillReport.class.getName()).log(Level.SEVERE, null, ex);
                        }                      
                    }                                         
                }                             
                private void Login() throws SQLException  {

                        JTextField username = new JTextField();
                        JTextField password = new JPasswordField();
                        Object[] message = {
                            "Username:", username,
                            "Password:", password
                         };

                        int option = JOptionPane.showConfirmDialog(null, message, "Login", JOptionPane.OK_CANCEL_OPTION);
                        if (option == JOptionPane.OK_OPTION) {
//                            System.out.println("login");
                            
                            String pass = null,user = null;
                            int id = 0;
     
//                            System.out.println("login2");
                            
                            Statement state=Settings.con.createStatement();
                            ResultSet rs=state.executeQuery("select * from login");
//                             System.out.println("login3");
                            if(rs.next()){
                             
                               user=rs.getString("user_name");
                               pass=rs.getString("password");
                               id=rs.getInt(1);                           
                            }  
                        
                            if(username.getText().equals(user) && password.getText().equals(pass)) {

                                JOptionPane.showInternalMessageDialog(SingleBillReport.this, "Login successfull","Success",  JOptionPane.INFORMATION_MESSAGE);                      
                                    try {
                                        
                                        Person p =new Person();
                                        int row = jTable1.getSelectedRow();
                                         p =((BillingFilterTable)jTable1.getModel()).getPerson(row);

                                         Statement st=Settings.con.createStatement();
//                                         String room = null;
                                         ResultSet set=st.executeQuery("select room from member_lodge,members where member_lodge.member_id=members.id and members.id=" +p.id);
                                            if(set.next()){
                                                p.room=set.getString(1);

                                            }                                 
                                        Billing bill=new Billing(p);
                                        desktop.add(bill);

                                        Dimension desktopSize = desktop.getSize();
                                        Dimension jInternalFrameSize = bill.getSize();
                                        bill.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
                                                (desktopSize.height- jInternalFrameSize.height)/2);                                             
                                        bill.setVisible(true);
                                    } catch (SQLException ex) {

                                        Logger.getLogger(SingleBillReport.class.getName()).log(Level.SEVERE, null, ex);
                                    }

                            } else {

                                JOptionPane.showInternalMessageDialog(SingleBillReport.this,"Enter Valid Username and Password!", "Login Failed",  JOptionPane.ERROR_MESSAGE);
                            }
                        } else {

                               JOptionPane.showInternalMessageDialog(SingleBillReport.this,"Login cancelled", "Cancelled",  JOptionPane.CANCEL_OPTION);
                        }
                    }                              
            });
            
                jTable1.setDefaultRenderer(Button.class, new BillingReportRenderer());                             
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        serviceCombo = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        label1 = new java.awt.Label();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        staffCombo = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        invoiceText = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setClosable(true);
        setMaximizable(true);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Service");

        serviceCombo.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N

        jButton1.setFont(new java.awt.Font("SansSerif", 0, 15)); // NOI18N
        jButton1.setText("Submit");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTable1.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Invoice No.", "Service", "Amount", "Qty", "Staff", "Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setGridColor(new java.awt.Color(204, 204, 204));
        jTable1.setRowHeight(22);
        jScrollPane1.setViewportView(jTable1);

        label1.setAlignment(java.awt.Label.CENTER);
        label1.setBackground(new java.awt.Color(244, 144, 22));
        label1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        label1.setForeground(new java.awt.Color(255, 255, 255));
        label1.setText("SINGLE INVOICE REPORT");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Count : ");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jButton2.setFont(new java.awt.Font("SansSerif", 0, 15)); // NOI18N
        jButton2.setText("Print");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("Staff");

        staffCombo.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Invoice");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1029, Short.MAX_VALUE)
            .addComponent(label1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(2, 2, 2)
                        .addComponent(invoiceText, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1)
                        .addGap(1, 1, 1)
                        .addComponent(serviceCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addGap(3, 3, 3)
                        .addComponent(staffCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(424, 424, 424))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(serviceCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(staffCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(invoiceText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");
        int invoice = invoiceText.getText().equals("") ? 0 :Integer.parseInt(invoiceText.getText());
        String service=serviceCombo.getSelectedItem() == null ? "" :(String) serviceCombo.getSelectedItem();
        String staff=staffCombo.getSelectedItem() == null ? "" :(String) staffCombo.getSelectedItem();
        int sum=0;
               
            try{
                ArrayList<Person> billingfilter =new ArrayList<Person>();
                Statement stmt=Settings.con.createStatement();
                
                String query="select * from bill_items where id>0";
                
                if(invoice != 0) {
                     
                    query += " and bill_id ='"+invoice+"'";  
                    
                  }
                 if(serviceCombo.getSelectedIndex() != -1 && !service.equals("")) {
                     
                    query += " and service ='"+service+"'";  
                    
                  }
                 if(staffCombo.getSelectedIndex() != -1 && !staff.equals("")) {
                     
                    query += " and staff ='"+staff+"'";  
                    
                  }
                 ResultSet rs= stmt.executeQuery(query);
                   
                    while(rs.next()){
                        Person bill=new Person();
                        bill.id=rs.getInt("bill_id");
                        bill.name=rs.getString("service");
                        bill.amount=rs.getInt("amount");
                        bill.qty =rs.getInt("qty");
                        bill.staff=rs.getString("staff");
                        bill.total=rs.getFloat("total");
                        sum=sum+rs.getInt(5);
                        
                        billingfilter.add(bill);                    
                    }
                     SingleBillFilterTable bft=new SingleBillFilterTable(billingfilter);
                     jTable1.setModel(bft);
                     jLabel6.setText(Integer.toString(sum));
                        
        }catch(Exception ex){
             Logger.getLogger(SingleBillReport.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            // TODO add your handling code here:
            MessageFormat header=new MessageFormat("Billing Report");
            jTable1.print(JTable.PrintMode.FIT_WIDTH,header,null);
        } catch (PrinterException ex) {
            Logger.getLogger(SingleBillReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField invoiceText;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private java.awt.Label label1;
    private javax.swing.JComboBox<String> serviceCombo;
    private javax.swing.JComboBox<String> staffCombo;
    // End of variables declaration//GEN-END:variables
    public static List<String> populateServiceArray() {
        List<String> service = new ArrayList<String>();
        try {
            Statement stmt = Settings.con.createStatement();
            ResultSet rs=stmt.executeQuery("select name from services order by name asc");
            service.add("");
            while(rs.next()){
                 service.add(rs.getString(1));
            }
        } catch(SQLException e) {
            System.out.println(e);
        }
        return service;
    }

    public static List<String> populateStaffArray() {
        List<String> staff = new ArrayList<String>();
        try {
            Statement stmt = Settings.con.createStatement();
            ResultSet rs=stmt.executeQuery("select name from staff order by name asc");
            staff.add("");
            while(rs.next()){
                 staff.add(rs.getString(1));
            }
        } catch(SQLException e) {
            System.out.println(e);
        }
        return staff;
    }
    
    private void filterServiceCombo(List<String> array) {
        this.service_array = array;
        serviceCombo.setEditable(true);
        final JTextField textfield = (JTextField) serviceCombo.getEditor().getEditorComponent();
        textfield.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent ke) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        currentCaretPosition=textfield.getCaretPosition();
                        if(textfield.getSelectedText()==null)
                        {
                        textfield.setCaretPosition(0);
                        comboServiceFilter(textfield.getText());
                        textfield.setCaretPosition(currentCaretPosition);
                        }
                    }
                });
            }
        });
        textfield.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        comboServiceFilter("");
                    }
                });
            }
        }); 
    }
    
    public void comboServiceFilter(String enteredText) {
        List<String> filterArray= new ArrayList<String>();
        for (int i = 0; i < service_array.size(); i++) {
            if (service_array.get(i).toLowerCase().contains(enteredText.toLowerCase())) {
                filterArray.add(service_array.get(i));
            }


        }
        if (filterArray.size() > 0) {

            serviceCombo.setModel(new DefaultComboBoxModel(filterArray.toArray()));
            serviceCombo.setSelectedItem(enteredText);
            serviceCombo.showPopup();
        }
        else {
            serviceCombo.hidePopup();
        }
    }
    
    private void filterStaffCombo(List<String> array) {
        this.staff_array = array;
        staffCombo.setEditable(true);
        final JTextField textfield = (JTextField) staffCombo.getEditor().getEditorComponent();
        textfield.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent ke) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        currentCaretPosition=textfield.getCaretPosition();
                        if(textfield.getSelectedText()==null)
                        {
                        textfield.setCaretPosition(0);
                        comboStaffFilter(textfield.getText());
                        textfield.setCaretPosition(currentCaretPosition);
                        }
                    }
                });
            }
        });
        textfield.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        comboStaffFilter("");
                    }
                });
            }
        }); 
    }
    
    public void comboStaffFilter(String enteredText) {
        List<String> filterArray= new ArrayList<String>();
        for (int i = 0; i < staff_array.size(); i++) {
            if (staff_array.get(i).toLowerCase().contains(enteredText.toLowerCase())) {
                filterArray.add(staff_array.get(i));
            }


        }
        if (filterArray.size() > 0) {

            staffCombo.setModel(new DefaultComboBoxModel(filterArray.toArray()));
            staffCombo.setSelectedItem(enteredText);
            staffCombo.showPopup();
        }
        else {
            staffCombo.hidePopup();
        }
    }
}
