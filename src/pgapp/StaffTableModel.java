/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author bluroe
 */
public class StaffTableModel extends AbstractTableModel{

  
    private static final String[] columnNames={"Name","Gender","Address","Contact","Mail","", ""};
    private static final Class [] columnClasses={String.class,String.class,String.class,String.class,String.class,JButton.class,JButton.class};
    private ArrayList<Person> memberList;
    
    public void removeRow(int rowIndex) {
        memberList.remove(rowIndex);
        fireTableRowsDeleted(rowIndex,rowIndex);
    }
    
    public StaffTableModel(ArrayList<Person> persons) {
      this.memberList = persons;
    }
    
    public String getColumnName(int col){
        return columnNames[col];
    }
     
    public Class<?> getColumnClass(int col){
        return columnClasses[col];
    }
          
    public Person getPerson(int i){
        return memberList.get(i);
    }
    
    @Override
    public int getRowCount() {
        
        return memberList.size();
    }

    @Override
    public int getColumnCount() {
       return columnNames.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        System.out.println(memberList.get(i));
        Person person=memberList.get(i);
         
        
        switch (i1){             
            case 0:
                return person.name;
                
            case 1:
                return person.gender;
                
            case 2: 
                return person.address;
               
            case 3:
                return person.contact;
                
            case 4:
                return person.mail;
                
            case 5:
                return person;
                
            case 6:
                return person;
                
        }
        return "";
    } 
}
