/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JTextArea;

/**
 *
 * @author bluroe
 */
public class Receipt implements Printable {
	
    String name, phone, date;
    String[] service_array;
    int subTotal;
    float discount, tax, grand_total;
    ArrayList<Person> customerList;
    public Receipt(String name, String phone, String date,int subTotal, float discount, float tax, float grand_total, ArrayList<Person> customerList) {
        this.name = name;
        this.phone = phone;
        this.date = date;
        this.subTotal=subTotal;
        this.discount = discount;
        this.tax = tax;
        this.grand_total = grand_total;
        this.customerList = customerList;
    }
	
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        SimpleDateFormat dateFormat = new SimpleDateFormat();
        dateFormat.applyPattern("dd MMM yyyy");
        String line = "----------------------------------------";
        Graphics2D g2d = (Graphics2D) graphics;
        Font font = new Font("MONOSPACED", Font.BOLD, 9);

        if (pageIndex < 0 || pageIndex >= 1) {
            return Printable.NO_SUCH_PAGE;
        }
        JTextArea textarea = new JTextArea(10, 40);

        textarea.append("       STUDIOM Hair n Beauty\n");
        textarea.append("    AVK NAIR ROAD,");
        textarea.append("  TELLICHERRY\n");
        textarea.append("        +91 9999 999 999\n");
        textarea.append("            RECEIPT\n" + line + "\n");
        textarea.append("Name    : " + name + "\n");
        textarea.append("Phone   : " + phone + "\n" + line + "\n");
        for (Person customer : customerList) {
            textarea.append(customer.getService() + "\n");
            textarea.append("   " + customer.getAmount() + " * " + customer.getQty() + " : " + customer.getTotal() + "\n");
        }
        textarea.append(line + "\n");
        textarea.append("Sub Total   : " + subTotal + "\n");
        textarea.append("Discount    : " + discount + "\n");
        textarea.append("Tax         : " + tax + "\n");
        textarea.append("Grand Total : " + grand_total + "\n");
        textarea.append("          THANK YOU!!!" + "\n");
		
        textarea.setEditable(false);

        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
        g2d.setFont(font);
        
        
        try {
            g2d.drawImage(ImageIO.read(getClass().getResource("/res/inspire_64.png")), 50, 0, 100, 34, null);
        } catch (IOException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
        }
		
        int lineStart, lineEnd, lineNumber = 0;
        int lineCount = textarea.getLineCount();
        String strText = textarea.getText();
        while (lineCount != 0) {
            try {
                lineStart = textarea.getLineStartOffset(lineNumber);
                lineEnd = textarea.getLineEndOffset(lineNumber);
                strText = textarea.getText(lineStart, lineEnd - lineStart);
            } catch (Exception exception) {
                System.out.println("Printing error:" + exception);
            }

            g2d.drawString(strText, 1, (lineNumber + 1) * 18 + 36);
            lineNumber = lineNumber + 1;
            lineCount--;
        }
        return Printable.PAGE_EXISTS;
    }
}