/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author bluroe
 */
public class DueListTable extends AbstractTableModel{
    
        private static final String[] columnNames={"Room No.","Member Name","Due Amount"};
        private static final Class [] columnClasses={int.class,String.class,int.class};
        private ArrayList<Person> DueList;
        
        
        DueListTable(ArrayList<Person> due) {
       this.DueList=due;
        }
        
         public String getColumnName(int col){
        return  columnNames[col];
        }
    
        public Class<?> getColumnClass(int col){
            return columnClasses[col];
        }
          
        public Person getPerson(int i){
            return DueList.get(i);
         }
    

    @Override
    public int getRowCount() {
        return DueList.size();
    }

    @Override
    public int getColumnCount() {
         return columnNames.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
       
        Person dueamount=DueList.get(i);
        switch(i1){
        
            case 0:
                   return dueamount.room;
                   
            case 1:
                    return dueamount.name;
            
            case 2:
                    return dueamount.due;
                      
        }
      return "";
    }
    
}
