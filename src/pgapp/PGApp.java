/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author bluroe
 */
public class PGApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException, IOException {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//        UIManager.setLookAndFeel("com.jgoodies.looks.windows.WindowsLookAndFeel");

        Settings.loadSettings();
        Class.forName("com.mysql.jdbc.Driver");
        String conStr = "jdbc:mysql://" + Settings.HOST + ":" + Settings.PORT + "/" + Settings.DBAS;
        Connection con = DriverManager.getConnection(conStr, Settings.USER, Settings.PASS);
        Settings.con = con;
        NewJFrame frame = new NewJFrame();
        frame.setVisible(true);
        frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
    }
}