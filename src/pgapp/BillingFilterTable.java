/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.awt.Button;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author bluroe
 */
public class BillingFilterTable extends AbstractTableModel{
    
        private static final String[] columnNames={"Invoice No","Customer","Phone","Date","Sub Total","Discount", "Tax", "Grand Total","Payment Mode"};
        private static final Class [] columnClasses={int.class,String.class,String.class,String.class,int.class,Float.class,Float.class,Float.class,String.class};
        private ArrayList<Person> billingReport;
        
        BillingFilterTable(ArrayList<Person> billingfilter) {
            this.billingReport=billingfilter;            
        }
        
         public String getColumnName(int col){
            return  columnNames[col];
        }
    
        public Class<?> getColumnClass(int col){
            return columnClasses[col];
        }
          
        public Person getPerson(int i){
            return billingReport.get(i);
         }
    
    @Override
    public int getRowCount() {
        return billingReport.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
       Person billing=billingReport.get(i);
       switch(i1){
           case 0:
               return billing.id;
               
           case 1:
               return billing.name;
               
           case 2:
               return billing.contact;
           
           case 3:
               return billing.bill_date;
               
           case 4:
               return billing.sub_total;
               
           case 5:           
               return billing.discount;
               
           case 6:
               return billing.tax;
               
           case 7:
               return billing.total;
           case 8:
               return billing.mode;
       }
    
    return "";
    }
}
