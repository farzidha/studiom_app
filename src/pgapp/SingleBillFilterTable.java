/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.awt.Button;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author bluroe
 */
public class SingleBillFilterTable extends AbstractTableModel{
    
        private static final String[] columnNames={"Invoice No.","Service","Amount","Qty","Staff", "Total"};
        private static final Class [] columnClasses={int.class,String.class,int.class,int.class,String.class,int.class};
        private ArrayList<Person> billingReport;
        
        SingleBillFilterTable(ArrayList<Person> billingfilter) {
            this.billingReport=billingfilter;            
        }
        
         public String getColumnName(int col){
            return  columnNames[col];
        }
    
        public Class<?> getColumnClass(int col){
            return columnClasses[col];
        }
          
        public Person getPerson(int i){
            return billingReport.get(i);
         }
    
    @Override
    public int getRowCount() {
        return billingReport.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
       Person billing=billingReport.get(i);
       switch(i1){
           case 0:
               return billing.id;
               
           case 1:
               return billing.name;
               
           case 2:
               return billing.amount;
           
           case 3:
               return billing.qty;
               
           case 4:
               return billing.staff;
               
           case 5:           
               return billing.total;
       }
    
    return "";
    }
}
