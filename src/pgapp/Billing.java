/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterJob;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.beans.binding.DoubleBinding;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.ComboBoxEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.autocomplete.ObjectToStringConverter;
/**
 *
 * @author bluroe
 */
public class Billing extends javax.swing.JInternalFrame {

     float monthMaxDays;
      int count = 0;
      int month,year;
      
      int amount;
      int total;
      int r;
      private List<String> name_array;
      private List<String> service_array;
      private List<String> staff_array;
      private int currentCaretPosition=0;
      boolean edit_mode = false;
      int edit_id = 0;
    /**
     * Creates new form Billing
     */
        public Billing() throws SQLException {
            this(0,"");

        }

        Billing(Person p) throws SQLException {
                    setTitle("EDIT");
                    setLocation(600, 30);
                    edit_mode = true;
                    initComponents();
           
                    label1.setText("EDIT PROFILE");
                    personbill = p;                 
                    
                    InvoiceNoText.setText(String.valueOf(p.id));
                    String startDateString = p.bill_date;
                    DateFormat df = new SimpleDateFormat("dd/MM/yyyy"); 
                    Date startDate;
                    try {
                        startDate = df.parse(startDateString);
                        datePicker.setDate(startDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    edit_id = p.id;
                    filterNameCombo(populateMemberArray());
                    filterServiceCombo(populateServiceArray());
                    filterStaffCombo(populateStaffArray());
                    nameCombo.setSelectedItem(p.name);
                    phoneText.setText(p.contact);
                    subTotalText.setText(String.valueOf(p.sub_total));
                    discountText.setText(String.valueOf(p.discount));
                    taxText.setText(String.valueOf(p.tax));
                    totalText.setText(String.valueOf(p.total));
                    if(p.mode.equalsIgnoreCase("cash")) {
                        jRadioButton1.setSelected(true);
                    } else { 
                         jRadioButton2.setSelected(true);
                    }
                    try {
                        Statement stmt = Settings.con.createStatement();
                        ResultSet rs=stmt.executeQuery("select service,amount,qty,staff,total from bill_items where bill_id = " + p.id);
                        while(rs.next()){
                            Object[] row = { rs.getString(1), rs.getInt(2), rs.getInt(3), rs.getString(4),rs.getInt(5) };
                            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                            model.addRow(row);
                        }
                    } catch(SQLException ex) {
                        Logger.getLogger(MemberTable.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    buttonGroup1.add(jRadioButton1);
                    buttonGroup1.add(jRadioButton2);
        }
    
        Billing(Integer Mid,String room ) throws SQLException {
            setTitle("BILLING");
            setResizable(true);
            month = Calendar.getInstance().get(Calendar.MONTH);
            year = Calendar.getInstance().get(Calendar.YEAR);

            ZoneId zoneId=ZoneId.of("Indian/Mahe");
            LocalDate today=LocalDate.now(zoneId);
            
            LocalDate lastday=today.with(TemporalAdjusters.lastDayOfMonth());
            Statement st = Settings.con.createStatement();
            String query1="select datediff('"+lastday+"', '"+today+"')";

            ResultSet re=st.executeQuery(query1);
            while(re.next()){
                count=re.getInt(1);
            }

             Calendar c = Calendar.getInstance();
             monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);

            initComponents();
            datePicker.setFormats(new SimpleDateFormat("dd/MM/yyyy"));
            Date date = new Date();
            datePicker.setDate(date);
            Vector vector = new Vector();
            DefaultComboBoxModel model =new DefaultComboBoxModel(vector);
            String invoiceNo = getInvoiceNo();
            InvoiceNoText.setText(invoiceNo);
            filterNameCombo(populateMemberArray());
            filterServiceCombo(populateServiceArray());
            filterStaffCombo(populateStaffArray());
            
            buttonGroup1.add(jRadioButton1);
            buttonGroup1.add(jRadioButton2);
            jRadioButton1.setSelected(true);
        }
        
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        datePicker = new org.jdesktop.swingx.JXDatePicker();
        label1 = new java.awt.Label();
        jLabel3 = new javax.swing.JLabel();
        InvoiceNoText = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel12 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jSeparator2 = new javax.swing.JSeparator();
        jButton2 = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        subTotalText = new javax.swing.JTextField();
        discountText = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        taxText = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        totalText = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        percentageText = new javax.swing.JTextField();
        percTaxText = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        nameCombo = new javax.swing.JComboBox<>();
        serviceCombo = new javax.swing.JComboBox<>();
        phoneText = new javax.swing.JTextField();
        amtText = new javax.swing.JTextField();
        qtyText = new javax.swing.JTextField();
        staffCombo = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setClosable(true);

        jLabel1.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jLabel1.setText("Name");

        jLabel2.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jLabel2.setText("Date");

        datePicker.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        datePicker.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                datePickerActionPerformed(evt);
            }
        });

        label1.setAlignment(java.awt.Label.CENTER);
        label1.setBackground(new java.awt.Color(244, 144, 22));
        label1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        label1.setForeground(new java.awt.Color(255, 255, 255));
        label1.setText("BILLING");

        jLabel3.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jLabel3.setText("Invoice no.");

        InvoiceNoText.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        InvoiceNoText.setEnabled(false);
        InvoiceNoText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InvoiceNoTextActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jLabel9.setText("Phone no.");

        jLabel10.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jLabel10.setText("Service");

        jLabel11.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jLabel11.setText("Amount");

        jLabel12.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jLabel12.setText("Qty");

        jButton1.setText("Add");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTable1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Service", "Amount", "Qty", "Staff", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setGridColor(new java.awt.Color(204, 204, 204));
        jScrollPane1.setViewportView(jTable1);

        jButton2.setText("Remove");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jLabel14.setText("Sub Total");

        jLabel15.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jLabel15.setText("Discount");

        subTotalText.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        subTotalText.setToolTipText("");
        subTotalText.setEnabled(false);
        subTotalText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                subTotalTextActionPerformed(evt);
            }
        });
        subTotalText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                subTotalTextKeyReleased(evt);
            }
        });

        discountText.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        discountText.setToolTipText("Rs.");
        discountText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                discountTextActionPerformed(evt);
            }
        });
        discountText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                discountTextKeyReleased(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jLabel16.setText("Tax");

        taxText.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        taxText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                taxTextActionPerformed(evt);
            }
        });
        taxText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                taxTextKeyReleased(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jLabel17.setText("Total");

        totalText.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        totalText.setEnabled(false);
        totalText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totalTextActionPerformed(evt);
            }
        });

        jButton3.setText("Exit");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Save & Print");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText("Save");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setText("Clear");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        percentageText.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        percentageText.setToolTipText("%");
        percentageText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                percentageTextActionPerformed(evt);
            }
        });
        percentageText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                percentageTextKeyReleased(evt);
            }
        });

        percTaxText.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        percTaxText.setToolTipText("%");
        percTaxText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                percTaxTextActionPerformed(evt);
            }
        });
        percTaxText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                percTaxTextKeyReleased(evt);
            }
        });

        jLabel4.setText("Rs.");

        jLabel5.setText("%");

        jLabel6.setText("Rs.");

        jLabel7.setText("%");

        nameCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                nameComboItemStateChanged(evt);
            }
        });

        serviceCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                serviceComboItemStateChanged(evt);
            }
        });

        amtText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                amtTextActionPerformed(evt);
            }
        });

        qtyText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                qtyTextActionPerformed(evt);
            }
        });

        staffCombo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                staffComboItemStateChanged(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        jLabel13.setText("Staff");

        jLabel8.setText("Payment Mode");

        jRadioButton1.setText("Cash");

        jRadioButton2.setText("Card");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jRadioButton1)
                        .addGap(18, 18, 18)
                        .addComponent(jRadioButton2)))
                .addContainerGap(16, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton1)
                    .addComponent(jRadioButton2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(label1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addComponent(jLabel10)
                .addGap(82, 82, 82)
                .addComponent(jLabel11)
                .addGap(51, 51, 51)
                .addComponent(jLabel12)
                .addGap(91, 91, 91)
                .addComponent(jLabel13)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButton2)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(InvoiceNoText, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(nameCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(datePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel9)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(phoneText, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(serviceCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(amtText, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(qtyText, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(staffCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1))
                            .addComponent(jScrollPane1))
                        .addContainerGap(29, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16)
                    .addComponent(jLabel17))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totalText, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(taxText)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel6)
                            .addGap(6, 6, 6)
                            .addComponent(percTaxText, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel7))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(discountText, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel4)
                                    .addGap(6, 6, 6)
                                    .addComponent(percentageText, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(subTotalText))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel5))))
                .addGap(12, 12, 12))
            .addComponent(jSeparator1)
            .addComponent(jSeparator2)
            .addComponent(jSeparator3)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(InvoiceNoText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(datePicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel1)
                    .addComponent(nameCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(phoneText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(serviceCombo)
                    .addComponent(amtText)
                    .addComponent(qtyText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(staffCombo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(subTotalText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(percentageText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(discountText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(taxText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16)
                            .addComponent(percTaxText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addGap(7, 7, 7)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(totalText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(jButton4)
                    .addComponent(jButton5)
                    .addComponent(jButton6))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void InvoiceNoTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InvoiceNoTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_InvoiceNoTextActionPerformed

    private void datePickerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_datePickerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_datePickerActionPerformed

    private void subTotalTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_subTotalTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_subTotalTextActionPerformed

    private void discountTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_discountTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_discountTextActionPerformed

    private void taxTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_taxTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_taxTextActionPerformed

    private void totalTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totalTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_totalTextActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(serviceCombo.getSelectedItem() == null || serviceCombo.getSelectedItem().toString().trim().equals("")) {
            JOptionPane.showInternalMessageDialog(this, "Enter service.", "Warning", JOptionPane.WARNING_MESSAGE);
        } else if(amtText.getText() == null || amtText.getText().equals("")) {
            JOptionPane.showInternalMessageDialog(this, "Enter amount.", "Warning", JOptionPane.WARNING_MESSAGE);
        } else if(qtyText.getText() == null || qtyText.getText().equals("")) {
            JOptionPane.showInternalMessageDialog(this, "Enter quantity.", "Warning", JOptionPane.WARNING_MESSAGE);
        } else if(staffCombo.getSelectedItem() == null || staffCombo.getSelectedItem().toString().trim().equals("")) {
            JOptionPane.showInternalMessageDialog(this, "Enter staff.", "Warning", JOptionPane.WARNING_MESSAGE);
        } else {
            String service = serviceCombo.getSelectedItem().toString();
            int amount = Integer.parseInt(amtText.getText());
            int qty = Integer.parseInt(qtyText.getText());
            String staff = staffCombo.getSelectedItem().toString();
            int rowCount = jTable1.getRowCount();
            boolean exist = false;
            // Check against all entries
            for (int i = 0; i < rowCount; i++) {
                if(jTable1.getValueAt(i, 0).toString().equalsIgnoreCase(service)) {
                    exist = true;
                }
            }
            if(exist) {
                JOptionPane.showInternalMessageDialog(this, "Item already added", "Warning", JOptionPane.WARNING_MESSAGE); 
                serviceCombo.setSelectedIndex(-1);
                amtText.setText("");
                qtyText.setText("");
            } else {
                int total = amount * qty;
                Object[] row = { service, amount, qty, staff, total };
                DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
                model.addRow(row);
                int sub_total =getSum();
                subTotalText.setText(String.valueOf(sub_total));
                if(!percentageText.getText().equals("")) {
                    float perc = Float.parseFloat(percentageText.getText());
                    float disc = (sub_total * perc) / 100;
                    discountText.setText(String.valueOf(disc));
                }
                float discount = discountText.getText().equals("") ? 0 : Float.parseFloat(discountText.getText());
                if(!percTaxText.getText().equals("")) {
                    float percTax = Float.parseFloat(percTaxText.getText());
                    float tx = (sub_total * percTax) / 100;
                    taxText.setText(String.valueOf(tx));
                }
                float tax = taxText.getText().equals("") ? 0 : Float.parseFloat(taxText.getText());
                float full_total = sub_total - discount + tax;
                totalText.setText(String.valueOf(full_total));
                serviceCombo.setSelectedIndex(-1);
                amtText.setText("");
                qtyText.setText("");
                staffCombo.setSelectedIndex(-1);
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void subTotalTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_subTotalTextKeyReleased
        
    }//GEN-LAST:event_subTotalTextKeyReleased

    private void discountTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_discountTextKeyReleased
            int sub_total = subTotalText.getText().equals("") ? 0 : Integer.parseInt(subTotalText.getText());
            float discount = discountText.getText().equals("") ? 0 : Float.parseFloat(discountText.getText());
            float tax = taxText.getText().equals("") ? 0 : Float.parseFloat(taxText.getText());
//            float perc = 100 / (sub_total / discount);
//            percentageText.setText(String.valueOf(perc));
            percentageText.setText("");
            float full_total = sub_total - discount + tax;
            totalText.setText(String.valueOf(full_total));
    }//GEN-LAST:event_discountTextKeyReleased

    private void percentageTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_percentageTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_percentageTextActionPerformed

    private void percentageTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_percentageTextKeyReleased
        int sub_total = subTotalText.getText().equals("") ? 0 : Integer.parseInt(subTotalText.getText());
        float perc = percentageText.getText().equals("") ? 0 : Float.parseFloat(percentageText.getText());
        float disc = (sub_total * perc) / 100;
        discountText.setText(String.valueOf(disc));
        float discount = discountText.getText().equals("") ? 0 : Float.parseFloat(discountText.getText());
        float tax = taxText.getText().equals("") ? 0 : Float.parseFloat(taxText.getText());
        float full_total = sub_total - discount + tax;
        totalText.setText(String.valueOf(full_total));
    }//GEN-LAST:event_percentageTextKeyReleased

    private void taxTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_taxTextKeyReleased
        int sub_total = subTotalText.getText().equals("") ? 0 : Integer.parseInt(subTotalText.getText());
        float discount = discountText.getText().equals("") ? 0 : Float.parseFloat(discountText.getText());
        float tax = taxText.getText().equals("") ? 0 : Float.parseFloat(taxText.getText());
        percTaxText.setText("");
        float full_total = sub_total - discount + tax;
        totalText.setText(String.valueOf(full_total));
    }//GEN-LAST:event_taxTextKeyReleased

    private void percTaxTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_percTaxTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_percTaxTextActionPerformed

    private void percTaxTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_percTaxTextKeyReleased
        int sub_total = subTotalText.getText().equals("") ? 0 : Integer.parseInt(subTotalText.getText());
        float perc = percTaxText.getText().equals("") ? 0 : Float.parseFloat(percTaxText.getText());
        float disc = (sub_total * perc) / 100;
        taxText.setText(String.valueOf(disc));
        float discount = taxText.getText().equals("") ? 0 : Float.parseFloat(discountText.getText());
        float tax = taxText.getText().equals("") ? 0 : Float.parseFloat(taxText.getText());
        float full_total = sub_total - discount + tax;
        totalText.setText(String.valueOf(full_total));
    }//GEN-LAST:event_percTaxTextKeyReleased

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        int[] rows = jTable1.getSelectedRows();
        for(int i=0;i<rows.length;i++){
          model.removeRow(rows[i]-i);
        }
        int sub_total =getSum();
        subTotalText.setText(String.valueOf(sub_total));
        float perc = percentageText.getText().equals("") ? 0 : Float.parseFloat(percentageText.getText());
        float disc = (sub_total * perc) / 100;
        discountText.setText(String.valueOf(disc));
        float discount = discountText.getText().equals("") ? 0 : Float.parseFloat(discountText.getText());
        float percTax = percTaxText.getText().equals("") ? 0 : Float.parseFloat(percTaxText.getText());
        float tx = (sub_total * percTax) / 100;
        taxText.setText(String.valueOf(tx));
        float tax = taxText.getText().equals("") ? 0 : Float.parseFloat(taxText.getText());
        float full_total = sub_total - discount + tax;
        totalText.setText(String.valueOf(full_total));
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        model.setRowCount(0);
        subTotalText.setText("");
        discountText.setText("");
        percentageText.setText("");
        taxText.setText("");
        percTaxText.setText("");
        totalText.setText("");
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        try{
            Date date = datePicker.getDate();
            String name = String.valueOf(nameCombo.getSelectedItem());
            String phone = phoneText.getText();
            boolean cash=jRadioButton1.isSelected();
            if(date == null) {
                JOptionPane.showInternalMessageDialog(this, "Enter date.", "Warning", JOptionPane.WARNING_MESSAGE);
            } else if(nameCombo.getSelectedItem() == null || name.trim().equals("")) {
                JOptionPane.showInternalMessageDialog(this, "Enter name.", "Warning", JOptionPane.WARNING_MESSAGE);
            } else if(phone == null || phone.equals("")) {
                JOptionPane.showInternalMessageDialog(this, "Enter phone no.", "Warning", JOptionPane.WARNING_MESSAGE);
            } else if(subTotalText.getText() == null || subTotalText.getText().equals("")) {
                JOptionPane.showInternalMessageDialog(this, "Service not added.", "Warning", JOptionPane.WARNING_MESSAGE);
            } else {
                DateFormat oDateFormat = new SimpleDateFormat("dd/MM/yyy");
                String billDate = oDateFormat.format(date);
                String mode = cash ? "Cash" : "Card";
                int subTotal = Integer.parseInt(subTotalText.getText());
                float discount = discountText.getText().equals("") ? 0 : Float.parseFloat(discountText.getText());
                float tax = taxText.getText().equals("") ? 0 : Float.parseFloat(taxText.getText());
                float grand_total = Float.parseFloat(totalText.getText());
                PreparedStatement pst,pst1;
                String generatedColumns[] = { "ID" };
                long billId = 0;
                if(edit_mode) {
                    String querybill = "update billing set mem_name = '"+name+"', mem_phone = '"+phone+"', billing_date = '"+billDate+"', sub_total = "+subTotal+", discount = "+discount+", tax = "+tax+", total = "+grand_total+", pay_mode = '"+mode+"' where id =" + edit_id;
                    pst = Settings.con.prepareStatement(querybill, generatedColumns);
                    pst.executeUpdate();  
                    
                    Statement stmt=Settings.con.createStatement();
                    stmt.executeUpdate("delete from bill_items where bill_id=" + edit_id);
                    
                    int rows=jTable1.getRowCount();

                    for(int row = 0; row<rows; row++)
                    {   
                      String service =(String)jTable1.getValueAt(row, 0);
                      int amount = (Integer) jTable1.getValueAt(row, 1);
                      int qty = (Integer)jTable1.getValueAt(row, 2);
                      String staff = (String)jTable1.getValueAt(row, 3);
                      int total = (Integer)jTable1.getValueAt(row, 4);
                      String queryco = "Insert into bill_items(bill_id,service,amount,qty,staff, total) values ('"+edit_id+"','"+service+"','"+amount+"','"+qty+"','"+staff+"','"+total+"')";

                      pst1 = Settings.con.prepareStatement(queryco);
                      pst1.execute();     
                    }
                    JOptionPane.showInternalMessageDialog(this, "Invoice edited successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
                    this.dispose();
                }else {
                    String querybill = "Insert into billing(mem_name,mem_phone,billing_date,sub_total,discount,tax,total,pay_mode) values ('"+name+"','"+phone+"','"+billDate+"','"+subTotal+"','"+discount+"','"+tax+"','"+grand_total+"','"+mode+"')";
                    pst = Settings.con.prepareStatement(querybill, generatedColumns);
                    pst.executeUpdate();  
                    ResultSet rs = pst.getGeneratedKeys();

                    if (rs.next()) {
                        billId = rs.getLong(1);
                    }
                    int rows=jTable1.getRowCount();

                    for(int row = 0; row<rows; row++)
                    {   
                      String service =(String)jTable1.getValueAt(row, 0);
                      int amount = (Integer) jTable1.getValueAt(row, 1);
                      int qty = (Integer)jTable1.getValueAt(row, 2);
                      String staff = (String)jTable1.getValueAt(row, 3);
                      int total = (Integer)jTable1.getValueAt(row, 4);
                      String queryco = "Insert into bill_items(bill_id,service,amount,qty,staff,total) values ('"+billId+"','"+service+"','"+amount+"','"+qty+"','"+staff+"','"+total+"')";

                      pst1 = Settings.con.prepareStatement(queryco);
                      pst1.execute();     
                    }
                    sendSMS(phone, name, grand_total);
                    JOptionPane.showInternalMessageDialog(this, "Saved Successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
                    this.dispose();
                }
            }
      } catch(Exception e){
            JOptionPane.showMessageDialog(this,e.getMessage());
      }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void nameComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_nameComboItemStateChanged
        if(nameCombo.getSelectedIndex() != -1) {
            String str = String.valueOf(nameCombo.getSelectedItem());
            String[] temp;

            String sep= "~";
            /* given string will be split by the argument sep provided. */
            temp = str.split(sep);
            /* print substrings */
            nameCombo.setSelectedItem(temp[0]);
            phoneText.setText(temp[1]);
        }
    }//GEN-LAST:event_nameComboItemStateChanged

    private void amtTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_amtTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_amtTextActionPerformed

    private void qtyTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_qtyTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_qtyTextActionPerformed

    private void serviceComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_serviceComboItemStateChanged
        if(serviceCombo.getSelectedIndex() != -1) {
            amtText.setText("");
            qtyText.setText("");
            String str = String.valueOf(serviceCombo.getSelectedItem());
            String[] temp;

            String sep= "~";
            /* given string will be split by the argument sep provided. */
            temp = str.split(sep);
            /* print substrings */
            serviceCombo.setSelectedItem(temp[0]);
            amtText.setText(temp[1]);
        }
    }//GEN-LAST:event_serviceComboItemStateChanged

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        try{
            Date date = datePicker.getDate();
            String name = String.valueOf(nameCombo.getSelectedItem());
            String phone = phoneText.getText();
            boolean cash=jRadioButton1.isSelected();
            if(date == null) {
                JOptionPane.showInternalMessageDialog(this, "Enter date.", "Warning", JOptionPane.WARNING_MESSAGE);
            } else if(nameCombo.getSelectedItem() == null || name.trim().equals("")) {
                JOptionPane.showInternalMessageDialog(this, "Enter name.", "Warning", JOptionPane.WARNING_MESSAGE);
            } else if(phone == null || phone.equals("")) {
                JOptionPane.showInternalMessageDialog(this, "Enter phone no.", "Warning", JOptionPane.WARNING_MESSAGE);
            } else if(subTotalText.getText() == null || subTotalText.getText().equals("")) {
                JOptionPane.showInternalMessageDialog(this, "Service not added.", "Warning", JOptionPane.WARNING_MESSAGE);
            } else {
                DateFormat oDateFormat = new SimpleDateFormat("dd/MM/yyy");
                String billDate = oDateFormat.format(date);
                int subTotal = Integer.parseInt(subTotalText.getText());
                float discount = discountText.getText().equals("") ? 0 : Float.parseFloat(discountText.getText());
                float tax = taxText.getText().equals("") ? 0 : Float.parseFloat(taxText.getText());
                float grand_total = Float.parseFloat(totalText.getText());
                String mode = cash ? "Cash" : "Card";
                PreparedStatement pst,pst1;
                String generatedColumns[] = { "ID" };
                long billId = 0;
                if(edit_mode) {
                    String querybill = "update billing set mem_name = '"+name+"', mem_phone = '"+phone+"', billing_date = '"+billDate+"', sub_total = "+subTotal+", discount = "+discount+", tax = "+tax+", total = "+grand_total+", pay_mode = '"+mode+"' where id =" + edit_id;
                    pst = Settings.con.prepareStatement(querybill, generatedColumns);
                    pst.executeUpdate();  
                    
                    Statement stmt=Settings.con.createStatement();
                    stmt.executeUpdate("delete from bill_items where bill_id=" + edit_id);
                    
                    int rows=jTable1.getRowCount();
                    ArrayList<Person> customerList = new ArrayList<>();
                    for(int row = 0; row<rows; row++)
                    {   
                        String service =(String)jTable1.getValueAt(row, 0);
                        int amount = (Integer) jTable1.getValueAt(row, 1);
                        int qty = (Integer)jTable1.getValueAt(row, 2);
                        String staff = (String)jTable1.getValueAt(row, 3);
                        int total = (Integer)jTable1.getValueAt(row, 4);
                        String queryco = "Insert into bill_items(bill_id,service,amount,qty,staff,total) values ('"+edit_id+"','"+service+"','"+amount+"','"+qty+"','"+staff+"','"+total+"')";

                        pst1 = Settings.con.prepareStatement(queryco);
                        pst1.execute();
                        Person customer = new Person(service, amount, qty, total);
                        customerList.add(customer);
                    }
                    PageFormat format = new PageFormat();
                    Paper paper = new Paper();

                    double paperWidth = 3;//3.25
                    double paperHeight = 3.69;//11.69
                    double leftMargin = 1;
                    double rightMargin = 1;
                    double topMargin = 0;
                    double bottomMargin = 0.01;

                    paper.setSize(paperWidth * 200, paperHeight * 200);
                    paper.setImageableArea(leftMargin * 200, topMargin * 200,
                    (paperWidth - leftMargin - rightMargin) * 200,
                    (paperHeight - topMargin - bottomMargin) * 200);

                    format.setPaper(paper);

                    PrintRequestAttributeSet attr = new HashPrintRequestAttributeSet();
                    attr.add(OrientationRequested.PORTRAIT);
                    PrinterJob printerJob = PrinterJob.getPrinterJob();
    //                LocalDate from = new java.sql.Date(jXDatePicker1.getDate().getTime()).toLocalDate();
    //                LocalDate lastday=from.with(TemporalAdjusters.lastDayOfMonth());
                    Printable printable = new Receipt(name, phone, billDate, subTotal, discount, tax, grand_total,customerList);

//                    format = printerJob.validatePage(format);
                    if(printerJob.printDialog()) {
                        printerJob.setPrintable(printable, format);
                        try {
                            printerJob.print(attr);
                        } catch(Exception e) {}
                    }
                    JOptionPane.showInternalMessageDialog(this, "Invoice edited successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
                    this.dispose();
                }else {
                    String querybill = "Insert into billing(mem_name,mem_phone,billing_date,sub_total,discount,tax,total,pay_mode) values ('"+name+"','"+phone+"','"+billDate+"','"+subTotal+"','"+discount+"','"+tax+"','"+grand_total+"','"+mode+"')";
                    pst = Settings.con.prepareStatement(querybill, generatedColumns);
                    pst.executeUpdate();  
                    ResultSet rs = pst.getGeneratedKeys();

                    if (rs.next()) {
                        billId = rs.getLong(1);
                    }
                    int rows=jTable1.getRowCount();
                    ArrayList<Person> customerList = new ArrayList<>();
                    for(int row = 0; row<rows; row++)
                    {   
                        String service =(String)jTable1.getValueAt(row, 0);
                        int amount = (Integer) jTable1.getValueAt(row, 1);
                        int qty = (Integer)jTable1.getValueAt(row, 2);
                        String staff = (String)jTable1.getValueAt(row, 3);
                        int total = (Integer)jTable1.getValueAt(row, 4);
                        String queryco = "Insert into bill_items(bill_id,service,amount,qty,staff,total) values ('"+billId+"','"+service+"','"+amount+"','"+qty+"','"+staff+"','"+total+"')";

                        pst1 = Settings.con.prepareStatement(queryco);
                        pst1.execute();  
                        Person customer = new Person(service, amount, qty, total);
                        customerList.add(customer);
                        
                    }
                    PageFormat format = new PageFormat();
                    Paper paper = new Paper();

                    double paperWidth = 3;//3.25
                    double paperHeight = 3.69;//11.69
                    double leftMargin = 1;
                    double rightMargin = 1;
                    double topMargin = 0;
                    double bottomMargin = 0.01;

                    paper.setSize(paperWidth * 200, paperHeight * 200);
                    paper.setImageableArea(leftMargin * 200, topMargin * 200,
                    (paperWidth - leftMargin - rightMargin) * 200,
                    (paperHeight - topMargin - bottomMargin) * 200);

                    format.setPaper(paper);

                    PrintRequestAttributeSet attr = new HashPrintRequestAttributeSet();
                    attr.add(OrientationRequested.PORTRAIT);
                    PrinterJob printerJob = PrinterJob.getPrinterJob();
    //                LocalDate from = new java.sql.Date(jXDatePicker1.getDate().getTime()).toLocalDate();
    //                LocalDate lastday=from.with(TemporalAdjusters.lastDayOfMonth());
                    Printable printable = new Receipt(name, phone, billDate, subTotal, discount, tax, grand_total,customerList);

//                    format = printerJob.validatePage(format);
                    if(printerJob.printDialog()) {
                        printerJob.setPrintable(printable, format);
                        try {
                            printerJob.print(attr);
                        } catch(Exception e) {}
                    }
                    sendSMS(phone, name, grand_total);
                    JOptionPane.showInternalMessageDialog(this, "Billing Done", "Success", JOptionPane.INFORMATION_MESSAGE);
                    this.dispose();
                }
            }
      } catch(Exception e){
            JOptionPane.showMessageDialog(this,e.getMessage());
      }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void staffComboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_staffComboItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_staffComboItemStateChanged

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField InvoiceNoText;
    private javax.swing.JTextField amtText;
    private javax.swing.ButtonGroup buttonGroup1;
    private org.jdesktop.swingx.JXDatePicker datePicker;
    private javax.swing.JTextField discountText;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTable jTable1;
    private java.awt.Label label1;
    private javax.swing.JComboBox<String> nameCombo;
    private javax.swing.JTextField percTaxText;
    private javax.swing.JTextField percentageText;
    private javax.swing.JTextField phoneText;
    private javax.swing.JTextField qtyText;
    private javax.swing.JComboBox<String> serviceCombo;
    private javax.swing.JComboBox<String> staffCombo;
    private javax.swing.JTextField subTotalText;
    private javax.swing.JTextField taxText;
    private javax.swing.JTextField totalText;
    // End of variables declaration//GEN-END:variables
    Person personbill = null;

    private String getInvoiceNo() {
        String invoice="";
        try {
            Statement stmt = Settings.con.createStatement();
            ResultSet rs=stmt.executeQuery("select max(id) from billing");

            while(rs.next()){
                 int value = rs.getInt(1) + 1;
                 invoice = String.valueOf(value);
            }
        } catch(SQLException e) {
            System.out.println(e);
        }
        return invoice;
    }
    
    public int getSum() {
        int count = jTable1.getRowCount();
        int sum = 0;
        for(int i=0; i<count;i++) {
            if(jTable1.getValueAt(i, 4) != null) {
            sum = sum + Integer.parseInt(jTable1.getValueAt(i, 4).toString());
            }
        }
        return sum;
    }
   
    public static List<String> populateMemberArray() {
        List<String> customer = new ArrayList<String>();
        try {
            Statement stmt = Settings.con.createStatement();
            ResultSet rs=stmt.executeQuery("select name, contact from members order by name asc");
            customer.add("");
            while(rs.next()){
                 customer.add(rs.getString(1) + "~" + rs.getString(2));
            }
        } catch(SQLException e) {
            System.out.println(e);
        }
        return customer;
    }
    
    public static List<String> populateServiceArray() {
        List<String> service = new ArrayList<String>();
        try {
            Statement stmt = Settings.con.createStatement();
            ResultSet rs=stmt.executeQuery("select name, price from services order by name asc");
            service.add("");
            while(rs.next()){
                 service.add(rs.getString(1)+"~"+rs.getInt(2));
            }
        } catch(SQLException e) {
            System.out.println(e);
        }
        return service;
    }

    public static List<String> populateStaffArray() {
        List<String> staff = new ArrayList<String>();
        try {
            Statement stmt = Settings.con.createStatement();
            ResultSet rs=stmt.executeQuery("select name from staff order by name asc");
            staff.add("");
            while(rs.next()){
                 staff.add(rs.getString(1));
            }
        } catch(SQLException e) {
            System.out.println(e);
        }
        return staff;
    }
    
    private void filterNameCombo(List<String> array) {
        this.name_array = array;
        nameCombo.setEditable(true);
        final JTextField textfield = (JTextField) nameCombo.getEditor().getEditorComponent();
        textfield.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent ke) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        currentCaretPosition=textfield.getCaretPosition();
                        if(textfield.getSelectedText()==null)
                        {
                        textfield.setCaretPosition(0);
                        comboNameFilter(textfield.getText());
                        textfield.setCaretPosition(currentCaretPosition);
                        }
                    }
                });
            }
        });
        textfield.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        comboNameFilter("");
                    }
                });
            }
        }); 
    }
    
    public void comboNameFilter(String enteredText) {
        List<String> filterArray= new ArrayList<String>();
        for (int i = 0; i < name_array.size(); i++) {
            if (name_array.get(i).toLowerCase().contains(enteredText.toLowerCase())) {
                filterArray.add(name_array.get(i));
            }


        }
        if (filterArray.size() > 0) {

            nameCombo.setModel(new DefaultComboBoxModel(filterArray.toArray()));
            nameCombo.setSelectedItem(enteredText);
            nameCombo.showPopup();
        }
        else {
            nameCombo.hidePopup();
        }
    }
    
    private void filterServiceCombo(List<String> array) {
        this.service_array = array;
        serviceCombo.setEditable(true);
        final JTextField textfield = (JTextField) serviceCombo.getEditor().getEditorComponent();
        textfield.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent ke) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        currentCaretPosition=textfield.getCaretPosition();
                        if(textfield.getSelectedText()==null)
                        {
                        textfield.setCaretPosition(0);
                        comboServiceFilter(textfield.getText());
                        textfield.setCaretPosition(currentCaretPosition);
                        }
                    }
                });
            }
        });
        textfield.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        comboServiceFilter("");
                    }
                });
            }
        }); 
    }
    
    public void comboServiceFilter(String enteredText) {
        List<String> filterArray= new ArrayList<String>();
        for (int i = 0; i < service_array.size(); i++) {
            if (service_array.get(i).toLowerCase().contains(enteredText.toLowerCase())) {
                filterArray.add(service_array.get(i));
            }


        }
        if (filterArray.size() > 0) {

            serviceCombo.setModel(new DefaultComboBoxModel(filterArray.toArray()));
            serviceCombo.setSelectedItem(enteredText);
            serviceCombo.showPopup();
        }
        else {
            serviceCombo.hidePopup();
        }
    }
    
    private void filterStaffCombo(List<String> array) {
        this.staff_array = array;
        staffCombo.setEditable(true);
        final JTextField textfield = (JTextField) staffCombo.getEditor().getEditorComponent();
        textfield.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent ke) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        currentCaretPosition=textfield.getCaretPosition();
                        if(textfield.getSelectedText()==null)
                        {
                        textfield.setCaretPosition(0);
                        comboStaffFilter(textfield.getText());
                        textfield.setCaretPosition(currentCaretPosition);
                        }
                    }
                });
            }
        });
        textfield.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        comboStaffFilter("");
                    }
                });
            }
        }); 
    }
    
    public void comboStaffFilter(String enteredText) {
        List<String> filterArray= new ArrayList<String>();
        for (int i = 0; i < staff_array.size(); i++) {
            if (staff_array.get(i).toLowerCase().contains(enteredText.toLowerCase())) {
                filterArray.add(staff_array.get(i));
            }


        }
        if (filterArray.size() > 0) {

            staffCombo.setModel(new DefaultComboBoxModel(filterArray.toArray()));
            staffCombo.setSelectedItem(enteredText);
            staffCombo.showPopup();
        }
        else {
            staffCombo.hidePopup();
        }
    }

    private void sendSMS(String phone, String name, float total) {
        try 
            {
                String Message = "Hi " + name + ", Confirm you that Rs." +String.valueOf(total)+ " received by Studiom for the service. Thank You. Visit Again";
                String data="user=" + URLEncoder.encode("studiom", "UTF-8");
                data +="&password=" + URLEncoder.encode("studiom123.", "UTF-8");
                data +="&message=" + URLEncoder.encode(Message, "UTF-8");
                data +="&sender=" + URLEncoder.encode("STDIOM", "UTF-8");
                data +="&mobile=" + URLEncoder.encode(phone, "UTF-8");
                data +="&type=" + URLEncoder.encode("3", "UTF-8");
                URL url = new URL("http://login.bulksmsgateway.in/sendmessage.php?"+data);
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();
                // Get the response
                BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line;
                String sResult1="";
                while ((line = rd.readLine()) != null) 
                {
                // Process line...
                sResult1=sResult1+line+" ";
                }
                wr.close();
                rd.close();
            } 
            catch (Exception e)
            {
                System.out.println("Error SMS "+e);
            }   
    }
}