/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;


import com.mysql.jdbc.PreparedStatement;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

/**
 *
 * @author bluroe
 */
public class NewJFrame extends javax.swing.JFrame {
     
    /**
     * Creates new form NewJFrame
     */
    
    public NewJFrame() {
       
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        List<Image> icons = new ArrayList<Image>();
        icons.add(toolkit.getImage(getClass().getResource("/res/inspire_16.png")));
        icons.add(toolkit.getImage(getClass().getResource("/res/inspire_32.png")));
        icons.add(toolkit.getImage(getClass().getResource("/res/inspire_64.png")));
        setIconImages(icons);
        initComponents();

        JMenuItem jMenuItem2 = new javax.swing.JMenuItem();
        
        jMenu2.setText("Edit");
        jMenuItem2.setText("Change Password");
        jMenu2.add(jMenuItem2);
        setJMenuBar(jMenuBar1);
        
        
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
                
                JTextField new_password = new JPasswordField();
                JTextField confirm_password= new JPasswordField();
                JTextField current_password = new JPasswordField();
                Object[] message = {
                            "Current Password:", current_password,
                            "New password:", new_password,
                            "Confirm New Password:", confirm_password
                         };

                int option = JOptionPane.showConfirmDialog(desktopPane, message, "Change Password", JOptionPane.OK_CANCEL_OPTION);              
                if (option == JOptionPane.OK_OPTION) {
                    
                    try {
                                  
                        String password=null;
                        int id = 0;
                        Statement stmt=Settings.con.createStatement();
                        ResultSet rs=stmt.executeQuery("select * from login");
                        if(rs.next()){
                           password=rs.getString("password");
                           id=rs.getInt(1);
                            
                        }
                        String new_pass=new_password.getText();
                        String confirm_pass=confirm_password.getText();
                        String current_pass=current_password.getText();
                        
                        if(!confirm_pass.equals(new_pass)){

                           JOptionPane.showInternalMessageDialog(desktopPane, "New Password Mismatch","Failed",  JOptionPane.ERROR_MESSAGE);

                        }
                        if(!current_pass.equals(password)){
                        
                            JOptionPane.showInternalMessageDialog(desktopPane, "Enter valid current password","Failed",  JOptionPane.ERROR_MESSAGE);
                        
                        }
                        if(confirm_pass.length()>4 || confirm_pass.length()<4){
                        
                            JOptionPane.showInternalMessageDialog(desktopPane, "Enter password having four characters","Failed",  JOptionPane.ERROR_MESSAGE);
                            
                        }                        
                        if(confirm_pass.equals(new_pass) && current_pass.equals(password) && confirm_pass.length()==4){
                            
                            PreparedStatement st=(PreparedStatement) Settings.con.prepareStatement("update login set password= ? where id=" +id);
                            st.setString(1, new_pass);                           
                            st.executeUpdate();
                            JOptionPane.showInternalMessageDialog(desktopPane, "Password Changed Successfully","Success",  JOptionPane.INFORMATION_MESSAGE);
                            
                        }
                       
                    } catch (SQLException ex) {
                        
                        Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
                        
                    }

                }else{
                        
                        JOptionPane.showInternalMessageDialog(desktopPane, "Cancelled", "Cancelled ", JOptionPane.CANCEL_OPTION);
                }           
            }
        });
        
        Action aboutAction = new AbstractAction() {
           public void actionPerformed(ActionEvent e) {
               jButton1ActionPerformed(e);
           } 
       };
         InputMap aboutMap = jButton1.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
            aboutMap.put(KeyStroke.getKeyStroke("F1"),"doSomething");
            jButton1.getActionMap().put("doSomething",aboutAction);
            
        Action memberAction = new AbstractAction() {
           public void actionPerformed(ActionEvent e) {
               jButton3ActionPerformed(e);
           } 
       };
         InputMap inputMap = jButton3.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
            inputMap.put(KeyStroke.getKeyStroke("F2"),"doSomething");
            jButton3.getActionMap().put("doSomething",memberAction);
            
        Action serviceAction = new AbstractAction() {
           public void actionPerformed(ActionEvent e) {
               SevicesActionPerformed(e);
           } 
       };
         InputMap serviceMap = Example1.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
            serviceMap.put(KeyStroke.getKeyStroke("F3"),"doSomething");
            Example1.getActionMap().put("doSomething",serviceAction);
           
            
        Action billingAction = new AbstractAction() {
           public void actionPerformed(ActionEvent e) {
               ExampleActionPerformed(e);
           } 
       };
         InputMap billingMap = Example.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
            billingMap.put(KeyStroke.getKeyStroke("F4"),"doSomething");
            Example.getActionMap().put("doSomething",billingAction);
        
       Action expenseAction = new AbstractAction() {
           public void actionPerformed(ActionEvent e) {
               jButton5ActionPerformed(e);
           } 
       };
         InputMap expenseMap = jButton5.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
            expenseMap.put(KeyStroke.getKeyStroke("F5"),"doSomething");
            jButton5.getActionMap().put("doSomething",expenseAction);   
            
        Action staffAction = new AbstractAction() {
           public void actionPerformed(ActionEvent e) {
               jButton8ActionPerformed(e);
           } 
       };
         InputMap staffMap = jButton8.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
            staffMap.put(KeyStroke.getKeyStroke("F6"),"doSomething");
            jButton8.getActionMap().put("doSomething",staffAction);   
       
        Action reportAction = new AbstractAction() {
           public void actionPerformed(ActionEvent e) {
               jButton6ActionPerformed(e);
           } 
       };
         InputMap reportMap = jButton6.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
            reportMap.put(KeyStroke.getKeyStroke("F7"),"doSomething");
            jButton6.getActionMap().put("doSomething",reportAction);   
       
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        jButton1 = new javax.swing.JButton();
        desktopPane = new javax.swing.JDesktopPane();
        jLabel1 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        Example = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        Example1 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("STUDIOM hair n beauty");

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/res/info-circle.png"))); // NOI18N
        jButton1.setText("About    ");
        jButton1.setFocusable(false);
        jButton1.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/res/variations-of-blue.jpg"))); // NOI18N
        desktopPane.add(jLabel1);
        jLabel1.setBounds(0, 0, 8000, 1440);

        jButton3.setBackground(new java.awt.Color(255, 255, 255));
        jButton3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/res/user-plus.png"))); // NOI18N
        jButton3.setText("Member    ");
        jButton3.setFocusable(false);
        jButton3.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        Example.setBackground(new java.awt.Color(255, 255, 255));
        Example.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        Example.setIcon(new javax.swing.ImageIcon(getClass().getResource("/res/rupee.png"))); // NOI18N
        Example.setText("Billing    ");
        Example.setFocusable(false);
        Example.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        Example.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        Example.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExampleActionPerformed(evt);
            }
        });

        jButton5.setBackground(new java.awt.Color(255, 255, 255));
        jButton5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/res/database.png"))); // NOI18N
        jButton5.setText("Expense  ");
        jButton5.setFocusable(false);
        jButton5.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jButton5.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setBackground(new java.awt.Color(255, 255, 255));
        jButton6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/res/file-text.png"))); // NOI18N
        jButton6.setText("Report    ");
        jButton6.setFocusable(false);
        jButton6.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jButton6.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jLabel2.setText("v1.2");

        Example1.setBackground(new java.awt.Color(255, 255, 255));
        Example1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        Example1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/res/building.png"))); // NOI18N
        Example1.setText("Services    ");
        Example1.setFocusable(false);
        Example1.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        Example1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        Example1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SevicesActionPerformed(evt);
            }
        });

        jButton8.setBackground(new java.awt.Color(255, 255, 255));
        jButton8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/res/user-plus.png"))); // NOI18N
        jButton8.setText("Staff    ");
        jButton8.setFocusable(false);
        jButton8.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jButton8.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jMenuBar1.setFocusable(false);

        jMenu1.setText("File");

        jMenuItem1.setText("Exit");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(jButton1)
                .addGap(3, 3, 3)
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Example1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Example)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 637, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addContainerGap())
            .addComponent(desktopPane)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(Example1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(Example, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jButton1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(5, 5, 5)
                .addComponent(desktopPane, javax.swing.GroupLayout.DEFAULT_SIZE, 767, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
       
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        About about;
        try {
            about = new About();
            about.setVisible(true);
            desktopPane.add(about);
            about.toFront();
        } catch (SQLException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
            
        }       
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        JPopupMenu menu = new JPopupMenu();
        JMenuItem m1, m2, m3;
        m1=new JMenuItem("Registration         F2   +");
        m2 = new JMenuItem("Members List");
        m3 = new JMenuItem("Send SMS");
        KeyStroke f1 = KeyStroke.getKeyStroke("1");
        m1.setAccelerator(f1);
        m1.addActionListener(e -> {
            if(registrationForm == null) {
                try {
                    registrationForm = new RegistrationForm();
                } catch (SQLException ex) {
                    Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
                registrationForm.addInternalFrameListener(new InternalFrameAdapter() {
                    @Override
                    public void internalFrameClosed(InternalFrameEvent e) {
                        super.internalFrameClosed(e);
                        registrationForm = null;
                    }               
                });
                registrationForm.desktop = this.desktopPane;
                registrationForm.setVisible(true);
                desktopPane.add(registrationForm);

                Dimension desktopSize = desktopPane.getSize();
                Dimension jInternalFrameSize = registrationForm.getSize();
                registrationForm.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
                (desktopSize.height- jInternalFrameSize.height)/2);

                registrationForm.toFront();
            }
        });
        m2.addActionListener(e -> {
            try {
                if(table == null) {
                    table = new MemberTable();     
                    table.addInternalFrameListener(new InternalFrameAdapter() {
                        @Override
                        public void internalFrameClosed(InternalFrameEvent e) {
                            super.internalFrameClosed(e);
                            table = null;
                        }               
                    });
                                  
                    table.desktop = desktopPane;
                    table.setVisible(true);
                    desktopPane.add(table);

                    Dimension desktopSize = desktopPane.getSize();
                    Dimension jInternalFrameSize = table.getSize();
                    table.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
                    (desktopSize.height- jInternalFrameSize.height)/2);

                    table.toFront();
                }
            } catch (SQLException ex) {

                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);

            } catch (IOException ex) {

                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);            
            } 
        });
        m3.addActionListener(e -> {
            if(smsForm == null) {
                try {
                    smsForm = new SmsForm();
                } catch (SQLException ex) {
                    Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
                smsForm.addInternalFrameListener(new InternalFrameAdapter() {
                    @Override
                    public void internalFrameClosed(InternalFrameEvent e) {
                        super.internalFrameClosed(e);
                        smsForm = null;
                    }               
                });
                smsForm.desktop = this.desktopPane;
                smsForm.setVisible(true);
                desktopPane.add(smsForm);

                Dimension desktopSize = desktopPane.getSize();
                Dimension jInternalFrameSize = smsForm.getSize();
                smsForm.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
                (desktopSize.height- jInternalFrameSize.height)/2);

                smsForm.toFront();
            }
        });
        menu.add(m1);
        menu.add(m2);
        menu.add(m3);
        Component b=(Component)evt.getSource();
        Point p=b.getLocationOnScreen();
        menu.show(this,0,0);
        menu.setLocation(p.x, p.y+b.getHeight());  
       
    }//GEN-LAST:event_jButton3ActionPerformed

    private void ExampleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExampleActionPerformed
                // TODO add your handling code here:
            
        JPopupMenu menu = new JPopupMenu();
        JMenuItem m1,m2;

        m1=new JMenuItem("Payment      F4   +");
        m2=new JMenuItem("View List");
        KeyStroke f1 = KeyStroke.getKeyStroke("1");
        m1.setAccelerator(f1);
        m1.addActionListener(e -> {
            try{
                if(bill == null) { 
                    bill=new Billing();
                    bill.addInternalFrameListener(new InternalFrameAdapter() {
                        @Override
                        public void internalFrameClosed(InternalFrameEvent e) {
                            super.internalFrameClosed(e);
                            bill = null;
                        }               
                    });
                    bill.setVisible(true);
                    desktopPane.add(bill);

                    Dimension desktopSize = desktopPane.getSize();
                    Dimension jInternalFrameSize = bill.getSize();
                    bill.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
                    (desktopSize.height- jInternalFrameSize.height)/2);

                    bill.toFront();
                }
            }
            catch(Exception ex)
            {
                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE,null,ex);
            }
        });
        m2.addActionListener(e -> {
            try {
                if(billingtable == null) {
                    billingtable = new BillingTable();
                    billingtable.addInternalFrameListener(new InternalFrameAdapter() {
                        @Override
                        public void internalFrameClosed(InternalFrameEvent e) {
                            super.internalFrameClosed(e);
                            billingtable = null;
                        }               
                    });
                    billingtable.desktop = desktopPane;
                    billingtable.setVisible(true);
                    desktopPane.add(billingtable);

                    Dimension desktopSize = desktopPane.getSize();
                    Dimension jInternalFrameSize = billingtable.getSize();
                    billingtable.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
                    (desktopSize.height- jInternalFrameSize.height)/2);

                    billingtable.toFront();
                }
            } catch (SQLException ex) {

                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);

            } catch (IOException ex) {

                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);            
            } 
        });
        menu.add(m1);
        menu.add(m2);
        Component b=(Component)evt.getSource();
        Point p=b.getLocationOnScreen();
        menu.show(this,0,0);
        menu.setLocation(p.x, p.y+b.getHeight());       
 
    }//GEN-LAST:event_ExampleActionPerformed
        
    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        JPopupMenu menu = new JPopupMenu();
        JMenuItem m1,m2,m3,m4;

        m1=new JMenuItem("Member Report");
        m1.addActionListener(e -> {
            MemberReport report=new MemberReport();
            report.setVisible(true);
            desktopPane.add(report);            
            Dimension desktopSize = desktopPane.getSize();
            Dimension jInternalFrameSize = report.getSize();
            report.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
            (desktopSize.height- jInternalFrameSize.height)/2);
            
            report.toFront();
             
        });
        m2=new JMenuItem("Billing Report");
        m2.addActionListener(e ->{
            BillingReport report=new BillingReport();
            report.desktop=this.desktopPane;
            report.setVisible(true);
            desktopPane.add(report);
            
            Dimension desktopSize = desktopPane.getSize();
            Dimension jInternalFrameSize = report.getSize();
            report.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
            (desktopSize.height- jInternalFrameSize.height)/2);
            
            report.toFront();
        
        });  
        m3=new JMenuItem("Expense Report");
        m3.addActionListener(e->{
            ExpenseReport expense=new ExpenseReport();
            expense.desktop=this.desktopPane;
            expense.setVisible(true);
            desktopPane.add(expense);
            
            Dimension desktopSize = desktopPane.getSize();
            Dimension jInternalFrameSize = expense.getSize();
            expense.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
            (desktopSize.height- jInternalFrameSize.height)/2);
            
            expense.toFront();    
        });
        m4=new JMenuItem("Single Bill Report");
        m4.addActionListener(e -> {
            SingleBillReport report=new SingleBillReport();
            report.desktop=this.desktopPane;
            report.setVisible(true);
            desktopPane.add(report);
            
            Dimension desktopSize = desktopPane.getSize();
            Dimension jInternalFrameSize = report.getSize();
            report.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
            (desktopSize.height- jInternalFrameSize.height)/2);
            
            report.toFront();
        });
        menu.add(m1);
        menu.add(m2); 
        menu.add(m3);
        menu.add(m4);
        Component b=(Component)evt.getSource();
        Point p=b.getLocationOnScreen();
        menu.show(this,0,0);
        menu.setLocation(p.x, p.y+b.getHeight());       
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        JPopupMenu menu = new JPopupMenu();
        JMenuItem m1, m2;

        m1=new JMenuItem("Create New    F5   +");
        m2 = new JMenuItem("Expenses List");
        KeyStroke f1 = KeyStroke.getKeyStroke("1");
        m1.setAccelerator(f1);
        m1.addActionListener(e -> {
            if(expense == null) {
                try {
                    expense = new Expense();
                } catch (SQLException ex) {
                    Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
                expense.addInternalFrameListener(new InternalFrameAdapter() {
                    @Override
                    public void internalFrameClosed(InternalFrameEvent e) {
                        super.internalFrameClosed(e);
                        expense = null;
                    }               
                });
                expense.desktop = this.desktopPane;
                expense.setVisible(true);
                desktopPane.add(expense);

                Dimension desktopSize = desktopPane.getSize();
                Dimension jInternalFrameSize = expense.getSize();
                expense.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
                (desktopSize.height- jInternalFrameSize.height)/2);

                expense.toFront();
            }
        });
        m2.addActionListener(e -> {
            try {
                if(expensetable == null) {
                    expensetable = new ExpenseTable();  
                    expensetable.addInternalFrameListener(new InternalFrameAdapter() {
                        @Override
                        public void internalFrameClosed(InternalFrameEvent e) {
                            super.internalFrameClosed(e);
                            expensetable = null;
                        }               
                    });
                    expensetable.desktop = desktopPane;
                    expensetable.setVisible(true);
                    desktopPane.add(expensetable);

                    Dimension desktopSize = desktopPane.getSize();
                    Dimension jInternalFrameSize = expensetable.getSize();
                    expensetable.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
                    (desktopSize.height- jInternalFrameSize.height)/2);

                    expensetable.toFront();
                }
            } catch (SQLException ex) {

                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);

            } catch (IOException ex) {

                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);            
            }
        });
        menu.add(m1);
        menu.add(m2);
        Component b=(Component)evt.getSource();
        Point p=b.getLocationOnScreen();
        menu.show(this,0,0);
        menu.setLocation(p.x, p.y+b.getHeight()); 
        
        
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        JPopupMenu menu = new JPopupMenu();
        JMenuItem m1, m2;

        m1=new JMenuItem("Create New     F6   +");
        m2 = new JMenuItem("Staff List");
        KeyStroke f1 = KeyStroke.getKeyStroke("1");
        m1.setAccelerator(f1);
        m1.addActionListener(e -> {
            if(staffForm == null) {
                try {
                    staffForm = new StaffForm();
                } catch (SQLException ex) {
                    Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
                staffForm.addInternalFrameListener(new InternalFrameAdapter() {
                    @Override
                    public void internalFrameClosed(InternalFrameEvent e) {
                        super.internalFrameClosed(e);
                        staffForm = null;
                    }               
                });
                staffForm.desktop = this.desktopPane;
                staffForm.setVisible(true);
                desktopPane.add(staffForm);

                Dimension desktopSize = desktopPane.getSize();
                Dimension jInternalFrameSize = staffForm.getSize();
                staffForm.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
                (desktopSize.height- jInternalFrameSize.height)/2);

                staffForm.toFront();
            }
        });
        m2.addActionListener(e -> {
            try {
                if(stafftable == null) {
                    stafftable = new StaffTable(); 
                    stafftable.addInternalFrameListener(new InternalFrameAdapter() {
                        @Override
                        public void internalFrameClosed(InternalFrameEvent e) {
                            super.internalFrameClosed(e);
                            stafftable = null;
                        }               
                    });
                    stafftable.desktop = desktopPane;
                    stafftable.setVisible(true);
                    desktopPane.add(stafftable);

                    Dimension desktopSize = desktopPane.getSize();
                    Dimension jInternalFrameSize = stafftable.getSize();
                    stafftable.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
                    (desktopSize.height- jInternalFrameSize.height)/2);

                    stafftable.toFront();
                }
            } catch (SQLException ex) {

                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);

            } catch (IOException ex) {

                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);            
            } 
        });
        menu.add(m1);
        menu.add(m2);
        Component b=(Component)evt.getSource();
        Point p=b.getLocationOnScreen();
        menu.show(this,0,0);
        menu.setLocation(p.x, p.y+b.getHeight()); 
    }//GEN-LAST:event_jButton8ActionPerformed

    private void SevicesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SevicesActionPerformed
        JPopupMenu menu = new JPopupMenu();
        JMenuItem m1, m2;

        m1=new JMenuItem("Create New      F3   +");
        m2 = new JMenuItem("Services List");
        
        KeyStroke f1 = KeyStroke.getKeyStroke("1");
        m1.setAccelerator(f1);
        
        m1.addActionListener(e -> {
            if(servicesForm == null) {
                try {
                    servicesForm = new ServicesForm();
                } catch (SQLException ex) {
                    Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
                servicesForm.addInternalFrameListener(new InternalFrameAdapter() {
                    @Override
                    public void internalFrameClosed(InternalFrameEvent e) {
                        super.internalFrameClosed(e);
                        servicesForm = null;
                    }               
                });
                servicesForm.desktop = this.desktopPane;
                servicesForm.setVisible(true);
                desktopPane.add(servicesForm);

                Dimension desktopSize = desktopPane.getSize();
                Dimension jInternalFrameSize = servicesForm.getSize();
                servicesForm.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
                (desktopSize.height- jInternalFrameSize.height)/2);

                servicesForm.toFront();
            }
        });
        m2.addActionListener(e -> {
            try {
                if(servicestable == null) {
                    servicestable = new ServicesTable();  
                    servicestable.addInternalFrameListener(new InternalFrameAdapter() {
                        @Override
                        public void internalFrameClosed(InternalFrameEvent e) {
                            super.internalFrameClosed(e);
                            servicestable = null;
                        }               
                    });
                    servicestable.desktop = desktopPane;
                    servicestable.setVisible(true);
                    desktopPane.add(servicestable);

                    Dimension desktopSize = desktopPane.getSize();
                    Dimension jInternalFrameSize = servicestable.getSize();
                    servicestable.setLocation((desktopSize.width - jInternalFrameSize.width)/2,
                    (desktopSize.height- jInternalFrameSize.height)/2);

                    servicestable.toFront();
                }
            } catch (SQLException ex) {

                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);

            } catch (IOException ex) {

                Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);            
            } 
        });
        menu.add(m1);
        menu.add(m2);
        Component b=(Component)evt.getSource();
        Point p=b.getLocationOnScreen();
        menu.show(this,0,0);
        menu.setLocation(p.x, p.y+b.getHeight());  
    }//GEN-LAST:event_SevicesActionPerformed

    public JDesktopPane getDesktop() {
        return desktopPane;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
         
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NewJFrame().setVisible(true);
              
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Example;
    private javax.swing.JButton Example1;
    private javax.swing.JDesktopPane desktopPane;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton8;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPopupMenu jPopupMenu1;
    // End of variables declaration//GEN-END:variables

    private SmsForm smsForm = null;
    private RegistrationForm registrationForm = null;
    private ServicesForm servicesForm = null;
    private Expense expense = null;
    private StaffForm staffForm = null;
    private Billing bill = null;
    private MemberTable table=null;
    private BillingTable billingtable=null;
    private ServicesTable servicestable=null;
    private StaffTable stafftable=null;
    private ExpenseTable expensetable=null;
    public static GridBagLayoutDemo grid=null;
}