/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author bluroe
 */
public class ServicesTableModel extends AbstractTableModel{

  
    private static final String[] columnNames={"Name","Price","",""};
    private static final Class [] columnClasses={String.class,Float.class,JButton.class,JButton.class};
    private ArrayList<Person> memberList;
   
    
    public ServicesTableModel(ArrayList<Person> persons) {
      this.memberList = persons;
    }
    
    public void removeRow(int rowIndex) {
        memberList.remove(rowIndex);
        fireTableRowsDeleted(rowIndex,rowIndex);
    }
    
    public String getColumnName(int col){
        return columnNames[col];
    }
     
    public Class<?> getColumnClass(int col){
        return columnClasses[col];
    }
          
    public Person getPerson(int i){
        return memberList.get(i);
    }
    
    @Override
    public int getRowCount() {
        
        return memberList.size();
    }

    @Override
    public int getColumnCount() {
       return columnNames.length;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Person person=memberList.get(i);
        
        int id=person.id;
        int payback = 0;
        
        
        
        switch (i1){              
            case 0:
                return person.name;
                
            case 1:
                return person.price;
                
            case 2:
                return person;
            case 3:
                return person;
                
        }
        return "";
    } 
}
