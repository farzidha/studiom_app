/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pgapp;

import com.mysql.jdbc.Blob;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.Date;


/**
 *
 * @author bluroe
 */
public class Person {
   
    int id;
    String name,guardian,address,proof_type,proof,status,mail, mode;
    float price, discount, tax, total;
    int rent, sub_total, qty;
    String room,room_no, bill_date;
    Date checkin,expcheckout,checkout;
    String guardaddress,work,contact;
    int bill_id;
    String type;
    Date date;
    int amount;
    String mem_name,gender;
    int due;
    int payback;
    Blob blob;
    InputStream stream;
    InputStream pic;
    BufferedImage img;
    BufferedImage image;
    Blob bl;
    String staff;
    int chmonth;
    int chyear;
    String bill_service;
    int bill_amt, bill_qty, bill_total;

    Person(String service, int amount, int qty, int total) {
        this.bill_service = service;
        this.bill_amt = amount;
        this.bill_qty = qty;
        this.bill_total = total;
    }

    Person() {
    }
   
    public String getService() {
        return bill_service;
    }
    public void setService(String service) {
        this.bill_service = service;
    }
    public int getAmount() {
        return bill_amt;
    }
    public void setAmount(int amount) {
        this.bill_amt = amount;
    }
    public int getQty() {
        return bill_qty;
    }
    public void setQty(int qty) {
        this.bill_qty = qty;
    }
    public int getTotal() {
        return bill_total;
    }
    public void setTotal(int total) {
        this.bill_total = total;
    }
    
}
