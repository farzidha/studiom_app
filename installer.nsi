!include "MUI2.nsh"

Name "Inspire PG App"
OutFile "installer.exe"
InstallDir "$PROGRAMFILES\pgapp"
!define MUI_ICON "Image\inspire_64.ico"

Function finishepageaction
	CreateShortcut "$DESKTOP\Inspire PG.lnk" "$INSTDIR\pgapp.exe"
FunctionEnd

!define MUI_FINISHPAGE_SHOWREADME ""
!define MUI_FINISHPAGE_SHOWREADME_TEXT "Create Desktop Shortcut"
!define MUI_FINISHPAGE_SHOWREADME_FUNCTION finishepageaction

var StartMenuFolder

!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

Section "Installer"

	SetOutPath "$INSTDIR"
	File "pgapp.exe"
	File "pgapp_console.exe"
	File "dist\PGApp.jar"
	File "config.properties"
	File "Image\inspire_16.ico"
	File "pg.sql"
	File "RentAlert.jar"
	CreateDirectory "$INSTDIR\files"
	CreateDirectory "$INSTDIR\lib"
	
	FileOpen $9 "$SMPROGRAMS\Startup\alert.bat" w ;Opens a Empty File an fills it
	FileWrite $9 'java -jar "$INSTDIR\RentAlert.jar"$\r$\n'
	FileClose $9 ;Closes the filled file
	
	SetOutPath "$INSTDIR\lib"
	File /r "dist\lib\*"
	WriteUninstaller "$INSTDIR\uninstall.exe"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\PGApp"  "DisplayName" "Inspire PG App"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\PGApp"  "Publisher" "Bluroe Labs"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\PGApp"  "DisplayIcon" "$INSTDIR\inspire_16.ico"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\PGApp"  "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
	!insertmacro MUI_STARTMENU_WRITE_BEGIN Application
		CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
		CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Inspire PG.lnk" "$INSTDIR\pgapp.exe"
		CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\uninstall.exe"
	!insertmacro MUI_STARTMENU_WRITE_END

	

SectionEnd

Section "Uninstall"

	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\PGApp"
	RMDir /r "$INSTDIR"
	!insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
	RMDir /r "$SMPROGRAMS\$StartMenuFolder"
	Delete "$DESKTOP\Inspire PG.lnk"
	Delete "$SMPROGRAMS\Startup\alert.bat"

SectionEnd