/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import pgapp.Settings;

/**
 *
 * @author bluroe
 */
public class NewEmptyJUnitTest {
    
    @Test
    public void hello() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        String conStr = "jdbc:mysql://localhost:3306/studiom_db";
        Connection con = DriverManager.getConnection(conStr, "root", "password");
        PreparedStatement stmt = con.prepareStatement("select * from floors");
        ResultSet rs = stmt.executeQuery();
        System.out.println(rs.next());
    }
}
